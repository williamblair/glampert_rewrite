#ifndef CUBE_H_INCLUDED
#define CUBE_H_INCLUDED

struct Cube
{
    static const u32 VERT_COUNT  = 24;
    static const u32 INDEX_COUNT = 36;

    DrawVertex * vertexes;
    u16     * indexes;
};

// ========================================================

Cube MakeCube(const PS2Vector & color, const float scale)
{
    // Face indexes:
    static const u16 cubeF[6][4] =
    {
        { 0, 1, 5, 4 }, { 4, 5, 6, 7 }, { 7, 6, 2, 3 },
        { 1, 0, 3, 2 }, { 1, 2, 6, 5 }, { 0, 4, 7, 3 }
    };

    // Positions/vertexes:
    static const float cubeV[8][3] =
    {
        { -0.5f, -0.5f, -0.5f },
        { -0.5f, -0.5f,  0.5f },
        {  0.5f, -0.5f,  0.5f },
        {  0.5f, -0.5f, -0.5f },

        { -0.5f,  0.5f, -0.5f },
        { -0.5f,  0.5f,  0.5f },
        {  0.5f,  0.5f,  0.5f },
        {  0.5f,  0.5f, -0.5f },
    };

    // Tex coords:
    static const float cubeT[4][2] =
    {
        { 0.0f, 0.0f },
        { 0.0f, 1.0f },
        { 1.0f, 1.0f },
        { 1.0f, 0.0f }
    };

    // Allocate a new data for a new cube:
    Cube cube;
    cube.vertexes = (DrawVertex*)memalign(16, Cube::VERT_COUNT*sizeof(DrawVertex));
    cube.indexes = (u16*)memalign(16, Cube::INDEX_COUNT*sizeof(u16));

    // Fill in the data:
    DrawVertex * vertexPtr = cube.vertexes;
    u16     * facePtr   = cube.indexes;

    // `i` iterates over the faces, 2 triangles per face:
    u16 vertIndex = 0;
    for (u32 i = 0; i < 6; ++i, vertIndex += 4)
    {
        for (u32 j = 0; j < 4; ++j)
        {
            vertexPtr->position.x = cubeV[cubeF[i][j]][0] * scale;
            vertexPtr->position.y = cubeV[cubeF[i][j]][1] * scale;
            vertexPtr->position.z = cubeV[cubeF[i][j]][2] * scale;
            vertexPtr->position.w = 1.0f;

            vertexPtr->texCoord.x = cubeT[j][0];
            vertexPtr->texCoord.y = cubeT[j][1];
            vertexPtr->texCoord.z = 0.0f;
            vertexPtr->texCoord.w = 1.0f;

            vertexPtr->color = color;
            ++vertexPtr;
        }

        facePtr[0] = vertIndex;
        facePtr[1] = vertIndex + 1;
        facePtr[2] = vertIndex + 2;
        facePtr += 3;

        facePtr[0] = vertIndex + 2;
        facePtr[1] = vertIndex + 3;
        facePtr[2] = vertIndex;
        facePtr += 3;
    }

    return cube;
}

void MakeCheckerTexture(Texture& texture)
{
    const int NUM_SQUARES = 8;
    const int IMG_SIZE = 256; // width = height = 256
    const int CHECKER_SIZE = IMG_SIZE / NUM_SQUARES;
    static Color4b data[IMG_SIZE * IMG_SIZE] __attribute__((aligned(16)));

    const Color4b pattern[] = 
    {
        {0,   0,   0,   255},
        {255, 255, 255, 255}
    };

    int startY = 0;
    int lastColor = 0;
    int color, rowX;

    while (startY < IMG_SIZE)
    {
        for (int y = startY; y < (startY + CHECKER_SIZE); y++)
        {
            color = lastColor;
            rowX = 0;
            for (int x = 0; x < IMG_SIZE; x++)
            {
                if (rowX == CHECKER_SIZE)
                {
                    color = !color;
                    rowX = 0;
                }
                data[x + y * IMG_SIZE] = pattern[color];
                rowX++;
            }
        }
        startY += CHECKER_SIZE;
        lastColor = !lastColor;
    }

    // non-mipmapped texture
    lod_t lod;
    lod.calculation = LOD_USE_K;
    lod.max_level = 0;
    lod.mag_filter = LOD_MAG_LINEAR;
    lod.min_filter = LOD_MIN_LINEAR;
    lod.l = 0;
    lod.k = 0.0f;
    if (!texture.InitFromMemory((u8*)data, 
                                TEXTURE_COMPONENTS_RGBA, 
                                IMG_SIZE, IMG_SIZE, 
                                GS_PSM_32, 
                                TEXTURE_FUNCTION_MODULATE, 
                                &lod))
    {
        printf("Failed to load checker texture\n");
    }
}

static inline void DrawCubeGroup(const Cube & cube, const float y)
{
    for (u32 z = 0; z < 3; ++z)
    {
        for (u32 x = 0; x < 3; ++x)
        {
            PS2Matrix trans, rotX, rotY;
            trans.makeTranslation((x * 5.0f) - 5.0f, y, (z * 5.0f) + 20.0f);
            rotX.makeRotationX(deg2rad(gCubeRotation));
            rotY.makeRotationY(deg2rad(gCubeRotation));

            renderer.SetModelMatrix(rotX * rotY * trans);
            renderer.DrawIndexedTriangles(cube.indexes, Cube::INDEX_COUNT, cube.vertexes, Cube::VERT_COUNT);
        }
    }
}

#endif // CUBE_H_INCLUDED
