#ifndef CAMERA_BASE_H_INCLUDED
#define CAMERA_BASE_H_INCLUDED

#include <PS2Pad.hpp>
#include <PS2Vector.h>
#include <PS2Matrix.h>

class CameraBase
{
public:

    virtual void Update(PS2Pad& gamePad, const PS2Vector& focusPoint) = 0;
    virtual PS2Matrix GetViewMatrix() const = 0;
    virtual PS2Vector GetCameraTarget() const = 0;
    
    virtual const PS2Vector& GetUpVec() const = 0;
    virtual const PS2Vector& GetRightVec() const = 0;
    virtual const PS2Vector& GetForwardVec() const = 0;
    virtual const PS2Vector& GetEyePosition() const = 0;
    
    virtual void SetEyePosition(const PS2Vector& eyePos) = 0;
    
    virtual bool IsThirdPerson() = 0;
    virtual bool IsFirstPerson() = 0;
};

#endif // CAMERA_BASE_H_INCLUDED
