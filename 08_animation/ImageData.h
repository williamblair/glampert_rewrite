#ifndef IMAGE_DATA_H_INCLUDED
#define IMAGE_DATA_H_INCLUDED


// used by Texture, TextureAtlas
struct ImageData
{
	u8*    pixels;
	u32    width;
	u32    height;
	u32    comps;
};


#endif // IMAGE_DATA_H_INCLUDED

