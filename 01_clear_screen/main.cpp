#include <kernel.h>
#include <tamtypes.h>
#include <climits>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <malloc.h>
#include <sifrpc.h>
#include <loadfile.h>
#include <libpad.h>
#include <stdio.h>
#include <draw.h>
#include <graph.h>
#include <gs_psm.h>
#include <cctype>
#include <math.h>
#include <dma.h>
#include <dma_tags.h>
#include <gif_tags.h>
#include <gs_gp.h>

#define BEGIN_DMA_TAG(qwordPtr) \
	qword_t * _dma_tag_ = (qwordPtr)++
#define END_DMA_TAG_AND_CHAIN(qwordPtr) \
	DMATAG_END(_dma_tag_, (qwordPtr) - _dma_tag_ - 1, 0, 0, 0); \
	_dma_tag_ = nullptr
#define END_DMA_TAG(qwordPtr) \
	DMATAG_CNT(_dma_tag_, (qwordPtr) - _dma_tag_ - 1, 0, 0, 0); \
	_dma_tag_ = nullptr

//
// Hardware constants:
//
enum
{
	// 16 Kilobytes (16384 bytes).
	SCRATCH_PAD_SIZE_BYTES = 0x4000,

	// 1024 128bit quadwords.
	SCRATCH_PAD_SIZE_QWORDS = SCRATCH_PAD_SIZE_BYTES / sizeof(qword_t),

	// The Scratch Pad (SPR) R/W memory is at a fixed address.
	SCRATCH_PAD_ADDRESS = 0x70000000,

	// ORing a pointer with this mask sets it to Uncached Accelerated (UCAB) space.
	UCAB_MEM_MASK = 0x30000000
};

class __attribute__((aligned(64))) RenderPacket
{
public:

    enum Type
    {
        NORMAL, // EE Ram
        UCAB, // Uncached Accelerated Memory (UCAB)
        SPR // Scratch Pad Memory
    };

    RenderPacket()
    {
        qwordBuff = NULL;
        qwordCount = 0;
        type = NORMAL;
    }
    RenderPacket( u32 quadWords, Type packetType )
    {
        qwordBuff = NULL;
        qwordCount = 0;
        type = NORMAL;

        Init( quadWords, packetType );
    }

    ~RenderPacket()
    {
        if ( qwordBuff == NULL ) {
            return;
        }

        if ( type == NORMAL )
        {
            free( qwordBuff );
        }
        else if ( type == UCAB )
        {
            u32 ucabAddr = (u32)qwordBuff;
            ucabAddr ^= UCAB_MEM_MASK;

            qwordBuff = (qword_t*)ucabAddr;

            free( qwordBuff );
        }
        else
        {
            // Scratch pad memory isn't freed;
            // is a fixed hardware buffer
        }

        qwordBuff = NULL;
    }

    void Init( u32 quadWords, Type packetType )
    {
        type = packetType;

        // Scratchpad memory
        if ( type == SPR )
        {
            if ( quadWords > SCRATCH_PAD_SIZE_QWORDS )
            {
                printf( "RenderPacket::Init: scratch pad mem num quadWords too large: %u > %u\n",
                       quadWords, SCRATCH_PAD_SIZE_QWORDS );
            }
            else
            {
                qwordBuff = (qword_t*)SCRATCH_PAD_ADDRESS;
                qwordCount = SCRATCH_PAD_SIZE_QWORDS;
            }
        }
        // Heap
        else
        {
            qwordBuff = (qword_t*)memalign( 64, quadWords );
            if ( qwordBuff == NULL )
            {
                printf( "RenderPacket::Init: failed to allocate from heap\n" );
            }
            qwordCount = quadWords;
        }
        // mark the pointer attribute to UCAB space
        if ( type == UCAB )
        {
            u32 ucabAddr = (u32)qwordBuff;
            ucabAddr |= UCAB_MEM_MASK;

            qwordBuff = (qword_t*)ucabAddr;
        }
    }

    qword_t* GetQwordPtr() const { return qwordBuff; }

    u32 GetQWordCount() const { return qwordCount; }

    u32 GetDisplacement( const qword_t* q ) const
    {
        //printf("Displacement: %d\n", (u32)(q - qwordBuff));
        return (u32)(q - qwordBuff);
    }

private:

    RenderPacket(const RenderPacket&);
    RenderPacket& operator = (const RenderPacket&);

    qword_t* qwordBuff; // Aligned to a cache line (`memAlloc(align=64)`)
    u32 qwordCount; // Size of `qwordBuff` in qword_t's
    Type type;
};

class Renderer
{
public:

    Renderer() :
        currentFramePacket(NULL),
        currentFrameQwPtr(NULL),
        dmaTagDraw2d(NULL),
        //currentTex(NULL),
        frameIndex(0),
        vramUserTextureStart(0),
        globalTextScale(1.0f),
        inMode2d(false),
        inMode3d(false)
    {}

    ~Renderer(){}

    bool Init(int width,
              int height,
              int vidMode,
              int fbPsm,
              int zPsm,
              bool interlaced)
    {
        // Reset the VRam pointer to the first address,
        graph_vram_clear();
        //gVramUsedBytes = 0;

        initGsBuffers(width, height, vidMode, fbPsm, zPsm, interlaced);
        initDrawingEnvironment();

        // create double buffer render packets
        //
        // FRAME_PACKET_SIZE is the number of quadwords per render packet
        // in the double buffer. 2 of them, so result is ~2MB memory
        //
        // No overflow checking is done - drawing large mesh could crash
        const u32 FRAME_PACKET_SIZE = 65535;
        framePackets[0].Init(FRAME_PACKET_SIZE, RenderPacket::NORMAL);
        framePackets[1].Init(FRAME_PACKET_SIZE, RenderPacket::NORMAL);

        // Extra packets for texture uploads
        textureUploadPacket[0].Init(128, RenderPacket::NORMAL);
        textureUploadPacket[1].Init(128, RenderPacket::NORMAL);

        // Small UCAB packet to send the flip buffer command
        flibFbPacket.Init(8, RenderPacket::UCAB);

        // Reset to be safe
        currentFramePacket = NULL;
        currentFrameQwPtr = NULL;
        // TODO
        //currentTex = NULL;
        frameIndex = 0;

        // Init other aux data and default render states
        primDesc.type = PRIM_TRIANGLE;
        primDesc.shading = PRIM_SHADE_GOURAUD;
        primDesc.mapping = DRAW_ENABLE;
        primDesc.fogging = DRAW_DISABLE;
        primDesc.blending = DRAW_DISABLE;
        primDesc.antialiasing = DRAW_DISABLE;
        primDesc.mapping_type = PRIM_MAP_ST;
        primDesc.colorfix = PRIM_UNFIXED;

        primColor.r = 255;
        primColor.g = 255;
        primColor.b = 255;
        primColor.a = 255;
        primColor.q = 1.0f;

        screenColor.r = 0;
        screenColor.g = 0;
        screenColor.b = 0;
        screenColor.a = 255;
        screenColor.q = 1.0f;

        inMode2d = false;
        inMode3d = false;

        //memset(&fps, 0, sizeof(fps));

        // This only really affects 2D drawing. It simply sets a flag
        // inside the library, which gets tested by the 2D drawing routines
        // So we can set this once on initialization and forget
        draw_enable_blending();

        printf("Renderer::Init() complete!\n");
        return true;
    }

    void BeginFrame()
    {
        printf("Frame Index: %d\n", frameIndex);
        currentFramePacket = &framePackets[frameIndex];
        currentFrameQwPtr = currentFramePacket->GetQwordPtr();
        printf("Frame QWPtr: 0x%X\n", (u32)currentFrameQwPtr);
    }

    void EndFrame()
    {
        // add finish command to DMA chain
        BEGIN_DMA_TAG(currentFrameQwPtr);
        currentFrameQwPtr = draw_finish(currentFrameQwPtr);
        END_DMA_TAG_AND_CHAIN(currentFrameQwPtr);

        dma_wait_fast();
        dma_channel_send_chain(DMA_CHANNEL_GIF, currentFramePacket->GetQwordPtr(), currentFramePacket->GetDisplacement(currentFrameQwPtr), 0, 0);

        // V-Sync wait
        graph_wait_vsync();
        draw_wait_finish();

        graph_set_framebuffer_filtered(framebuffers[frameIndex].address, framebuffers[frameIndex].width, framebuffers[frameIndex].psm, 0, 0);

        // switch context
        // 1 XOR 1 = 0
        // 0 XOR 1 = 1
        frameIndex ^= 1;

        // swap render framebuffer
        flipBuffers(framebuffers[frameIndex]);
    }

    void ClearScreen()
    {
        BEGIN_DMA_TAG(currentFrameQwPtr);

        currentFrameQwPtr = draw_disable_tests(currentFrameQwPtr, 0, &zbuff);

        const uint width = GetScreenWidth();
        const uint height = GetScreenHeight();

        printf("Screen width, height: %d, %d\n", width, height);

        currentFrameQwPtr = draw_clear(currentFrameQwPtr, 0, 2048 - (width / 2), 2048 - (height / 2), width, height, screenColor.r, screenColor.g, screenColor.b);
        currentFrameQwPtr = draw_enable_tests(currentFrameQwPtr, 0, &zbuff);

        END_DMA_TAG(currentFrameQwPtr);
    }

    void SetClearScreenColor(u8 r, u8 g, u8 b)
    {
        screenColor.r = r;
        screenColor.g = g;
        screenColor.b = b;
    }

    void SetGlobalTextScale( const float scale )
    {
        globalTextScale = scale;
    }

    void SetPrimTextureMapping( const bool enable )
    {
        if ( enable )
        {
            primDesc.mapping = DRAW_ENABLE;
            primDesc.mapping_type = PRIM_MAP_ST;
        }
        else
        {
            primDesc.mapping = DRAW_DISABLE;
            primDesc.mapping_type = DRAW_DISABLE;
        }
    }

    void SetPrimAntialiasing( const bool enable )
    {
        primDesc.antialiasing = enable ? DRAW_ENABLE : DRAW_DISABLE;
    }

    void SetPrimShading( const int shading )
    {
        primDesc.shading = shading;
    }

    float GetAspectRatio()
    {
        //return framebuffers[0].width / framebuffers[0].height;
        return 4.0f/3.0f;
    }

    u32 GetScreenWidth()
    {
        return framebuffers[0].width;
    }

    u32 GetScreenHeight()
    {
        return framebuffers[0].height;
    }

private:
    // Two packets/buffers for double buffering
    zbuffer_t zbuff;
    framebuffer_t framebuffers[2];
    RenderPacket framePackets[2];
    RenderPacket flibFbPacket;
    RenderPacket* currentFramePacket;
    qword_t* currentFrameQwPtr;
    qword_t* dmaTagDraw2d;
    // TODO
    // Texture* currentTex;
    u32 frameIndex;

    // Texture mapping aux data
    RenderPacket textureUploadPacket[2];
    int vramUserTextureStart;

    // 3D Primitive/geometry attributes
    prim_t primDesc;
    color_t primColor;
    color_t screenColor;

    // optional default scale for text glyphs. Initially no scale (1.0)
    float globalTextScale;

    // TODO
    // TextureAtlas texAtlas;

    // 2D Rendering, including text, can only take place between `begin2d()` and `end2d()` calls
    bool inMode2d;

    // 3D rendering can only take place between `begin3d()` and `end3d()` calls
    bool inMode3d;

    inline int vramAlloc(int width,
                         int height,
                         int psm,
                         int alignment)
    {
        const int addr = graph_vram_allocate(width, height, psm, alignment);
        if (addr < 0)
        {
            printf("Failed to allocate VRam space!\n");
        }

        return addr;
    }

    void initGsBuffers(int width,
                       int height,
                       int vidMode,
                       int fbPsm,
                       int zPsm,
                       bool interlaced)
    {
        // init GIF DMA channel
        dma_channel_initialize(DMA_CHANNEL_GIF, NULL, 0);
        dma_channel_fast_waits(DMA_CHANNEL_GIF);

        // frame buffer 0
        framebuffers[0].width = width;
        framebuffers[0].height = height;
        framebuffers[0].mask = 0;
        framebuffers[0].psm = fbPsm;
        framebuffers[0].address =
            vramAlloc(width, height, fbPsm, GRAPH_ALIGN_PAGE);

        // frame buffer 1
        framebuffers[1].width = width;
        framebuffers[1].height = height;
        framebuffers[1].mask = 0;
        framebuffers[1].psm = fbPsm;
        framebuffers[1].address =
            vramAlloc(width, height, fbPsm, GRAPH_ALIGN_PAGE);

        // Z/Depth buffer
        zbuff.enable = DRAW_ENABLE;
        zbuff.mask = 0;
        zbuff.method = ZTEST_METHOD_GREATER_EQUAL;
        zbuff.zsm = zPsm;
        zbuff.address =
            vramAlloc(width, height, zPsm, GRAPH_ALIGN_PAGE);

        // User textures start after the z-buffer
        // Allocate space for a single 256x256 large texture
        //  (pretty much all the space we have left)
        // TODO
        //vramUserTextureStart = vramAlloc(Texture::MAX_SIZE, Texture::MAX_SIZE, GS_PSM_32, GRAPH_ALIGN_BLOCK);
        vramUserTextureStart = vramAlloc(256, 256, GS_PSM_32, GRAPH_ALIGN_BLOCK);

        // initialize the screen and tie the first framebuffer to the read circuits

        if (vidMode == GRAPH_MODE_AUTO)
        {
            vidMode = graph_get_region();
        }

        // set video mode with flicker filter
        const int graphMode = interlaced ?
            GRAPH_MODE_INTERLACED :
            GRAPH_MODE_NONINTERLACED;
        graph_set_mode(graphMode, vidMode, GRAPH_MODE_FIELD, GRAPH_ENABLE);

        graph_set_screen(0, 0, width, height);
        graph_set_bgcolor(0, 0, 0);
        graph_set_framebuffer_filtered(framebuffers[0].address, framebuffers[0].width, framebuffers[0].psm, 0, 0);
        graph_enable_output();
    }

    void initDrawingEnvironment()
    {
        RenderPacket packet(50, RenderPacket::NORMAL);

        // set framebuffer and virtual screen offsets
        qword_t* q = packet.GetQwordPtr();
        q = draw_setup_environment(q, 0, &framebuffers[0], &zbuff);
        q = draw_primitive_xyoffset(q,
                                    0,
                                    2048 - (GetScreenWidth() / 2),
                                    2048 - (GetScreenHeight() / 2));


        // texture wrapping mode fixed to REPEAT
        texwrap_t wrap;
        wrap.horizontal = WRAP_REPEAT;
        wrap.vertical = WRAP_REPEAT;
        wrap.minu = wrap.maxu = 0;
        wrap.minv = wrap.maxv = 0;
        q = draw_texture_wrapping(q, 0, &wrap);

        q = draw_finish(q);
        dma_channel_send_normal(DMA_CHANNEL_GIF, packet.GetQwordPtr(), packet.GetDisplacement(q), 0, 0);
        dma_wait_fast();
    }

    void flipBuffers(framebuffer_t& fb)
    {
        qword_t* q = flibFbPacket.GetQwordPtr();

        q = draw_framebuffer(q, 0, &fb);
        q = draw_finish(q);

        dma_wait_fast();
        dma_channel_send_normal_ucab(DMA_CHANNEL_GIF, flibFbPacket.GetQwordPtr(), flibFbPacket.GetDisplacement(q), 0);
        draw_wait_finish();
    }
};

static inline double deg2rad( const double deg )
{
    return deg * (3.141592653589793f / 180.0f);
}

Renderer renderer;

int main(int argc, char* argv[])
{
    SifInitRpc(0);

    if (!renderer.Init( 640, 448,         // 640x448 is standard NTSC resolution
                   GRAPH_MODE_AUTO,  // Select best mode (PAL or NTSC)
                   GS_PSM_32,        // Framebuffer color format
                   GS_PSMZ_32,       // Z-buffer format
                   true ))            // Interlace ON
    {
        printf("Failed to init graphics\n");
        while (1)
        {
        }
    }

    printf("End renderer init\n");

    renderer.SetClearScreenColor( 65, 25, 0 );
    renderer.SetGlobalTextScale( 0.67f );
    renderer.SetPrimTextureMapping( true );
    //renderer.setPrimTextureMapping( false );
    renderer.SetPrimAntialiasing( true );
    renderer.SetPrimShading( PRIM_SHADE_FLAT );

    // gConsole.preloadFonts();

    for (;;)
    {
        // TODO
        //gTime.beginFrame();

        // TODO
        // gPS2Pad.read();
        // padInput();

        printf("Begin frame\n");
        renderer.BeginFrame();
        //printf("Clear screen\n");
        renderer.ClearScreen();
        printf("End frame\n");
        renderer.EndFrame();
        printf("end loop\n");
    }

    SleepThread();

    return 0;
}
