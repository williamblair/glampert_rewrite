#ifndef TEXTURE_H_INCLUDED
#define TEXTURE_H_INCLUDED

#include <array.hpp>

// PS2DEV SDK:
#include <draw.h>
#include <graph.h>
#include <gs_psm.h>

class Texture
{
public:

    // max square size is 256x256x4 (4 is RGBA)
    static const u32 MAX_SIZE = 256;
    static u32 vramUserTextureStart;

    // constructor
    Texture() :
        texData(NULL),
        texHeight(0),
        texBuf(),
        texLod(),
        texClut()
    {}

    bool InitEmpty(u8 comps,
                   u32 w,
                   u32 h,
                   u32 psm,
                   u8 func,
                   const lod_t* lod = NULL,
                   const clutbuffer_t* clut = NULL)
    {
        // TODO
        //if (w == 0) {ERROR}
        //if (h == 0) {ERROR}

        if (w > MAX_SIZE) {
            printf("Texture width greater than max %d: %d\n", MAX_SIZE, w);
        }
        if (h > MAX_SIZE) {
            printf("Texture height greater than max %d: %d\n", MAX_SIZE, h);
        }

        texHeight = h;
        texBuf.width = w;
        texBuf.psm = psm;
        texBuf.info.width = draw_log2(w);
        texBuf.info.height = draw_log2(h);
        texBuf.info.components = comps;
        texBuf.info.function = func;
        
        // All textures must share the same VRam space. Only enough VRam left
        // for a single texture at a time. Every texture must be copied into
        // vram on the fly before use
        //texBuf.address = gRenderer.getVRamUserTextureStart();
        texBuf.address = vramUserTextureStart;

        if (lod != NULL)
        {
            texLod = *lod;
        }
        else
        {
            // defaults
            texLod.calculation = LOD_USE_K;
            texLod.max_level = 0;
            texLod.mag_filter = LOD_MAG_NEAREST;
            texLod.min_filter = LOD_MIN_NEAREST;
            texLod.l = 0;
            texLod.k = 0.0f;
        }

        if (clut != NULL)
        {
            texClut = *clut;
        }
        else
        {
            // defaults
            texClut.address = 0;
            texClut.psm = 0;
            texClut.storage_mode = CLUT_STORAGE_MODE1;
            texClut.start = 0;
            texClut.load_method = CLUT_NO_LOAD;
        }

        return true;
    }

    // Init from existing
    bool InitFromMemory(const u8* data,
                        u8 comps,
                        u32 width,
                        u32 height,
                        u32 psm,
                        u8 func,
                        const lod_t* lod = NULL,
                        const clutbuffer_t* clut = NULL)
    {
        if (!InitEmpty(comps, width, height, psm, func, lod, clut))
        {
            return false;
        }

        texData = data;
        return true;
    }

    const u8* GetPixels() const
    {
        return texData;
    }
    void SetPixels(const u8* pixels)
    {
        texData = pixels;
    }

    texbuffer_t& GetTexBuffer() { return texBuf; }
    clutbuffer_t& GetTexClut() { return texClut; }
    lod_t& GetTexLod() { return texLod; }

    u32 GetWidth() const
    {
        return texBuf.width;
    }
    u32 GetHeight() const
    {
        return texHeight;
    }
    u32 GetPixelFormat() const
    {
        return texBuf.psm;
    }

private:

    // no copy
    Texture(const Texture&);
    Texture& operator = (const Texture&);

    const u8* texData; // external data ptr
    u32 texHeight;
    texbuffer_t texBuf;
    lod_t texLod;
    clutbuffer_t texClut;
};

#endif // TEXTURE_H_INCLUDED

