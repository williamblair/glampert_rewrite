#ifndef RENDERER_H_INCLUDED
#define RENDERER_H_INCLUDED

#include <Texture.h>
#include <TextureAtlas.h>
#include <FontManager.h>

#define BEGIN_DMA_TAG(qwordPtr) \
	qword_t * _dma_tag_ = (qwordPtr)++
#define BEGIN_DMA_TAG_NAMED(tagName, qwordPtr) \
	tagName = (qwordPtr)++
#define END_DMA_TAG_AND_CHAIN(qwordPtr) \
	DMATAG_END(_dma_tag_, (qwordPtr) - _dma_tag_ - 1, 0, 0, 0); \
	_dma_tag_ = nullptr
#define END_DMA_TAG(qwordPtr) \
	DMATAG_CNT(_dma_tag_, (qwordPtr) - _dma_tag_ - 1, 0, 0, 0); \
	_dma_tag_ = nullptr
#define END_DMA_TAG_NAMED(tagName, qwordPtr) \
	DMATAG_CNT((tagName), (qwordPtr) - (tagName) - 1, 0, 0, 0); \
	(tagName) = nullptr

static inline bool cullBackFacingTriangle(const PS2Vector * eye, const PS2Vector * v0,
                                          const PS2Vector * v1,  const PS2Vector * v2)
{
	// Plane equation of the triangle will give us its orientation
	// which can be compared with the viewer's world position.
	//
	// const Vector d = crossProduct(*v2 - *v0, *v1 - *v0);
	// const Vector c = *v0 - *eye;
	// return dotProduct3(c, d) <= 0.0f;
	//
	register float dot;
	asm volatile (
		"lqc2        vf4, 0x0(%1)  \n\t" // vf4 = eye
		"lqc2        vf5, 0x0(%2)  \n\t" // vf5 = v0
		"lqc2        vf6, 0x0(%3)  \n\t" // vf6 = v1
		"lqc2        vf7, 0x0(%4)  \n\t" // vf7 = v2
		"vsub.xyz    vf8, vf7, vf5 \n\t" // vf8 = vf7(v2) - vf5(v0)
		"vsub.xyz    vf9, vf6, vf5 \n\t" // vf9 = vf6(v1) - vf5(v0)
		"vopmula.xyz ACC, vf8, vf9 \n\t" // vf6 = cross(vf8, vf9)
		"vopmsub.xyz vf6, vf9, vf8 \n\t"
		"vsub.w      vf6, vf6, vf6 \n\t"
		"vsub.xyz    vf7, vf5, vf4 \n\t" // vf7 = vf5(v0) - vf4(eye)
		"vmul.xyz    vf5, vf7, vf6 \n\t" // vf5 = dot(vf7, vf6)
		"vaddy.x     vf5, vf5, vf5 \n\t"
		"vaddz.x     vf5, vf5, vf5 \n\t"
		"qmfc2       $2,  vf5      \n\t" // store result on `dot` variable
		"mtc1        $2,  %0       \n\t"
		: "=f" (dot)
		: "r" (eye), "r" (v0), "r" (v1), "r" (v2)
		: "$2"
	);
	return dot <= 0.0f;
}


struct __attribute__((aligned(16))) DrawVertex
{
    PS2Vector position; // only x,y,z used; w = 1
    PS2Vector texCoord; // only x,y used; z = 0, w = 1
    PS2Vector color; // XYZW used to store RGBA
};

static inline void applyXForm(PS2Vector * vOut, const PS2Matrix * m, const PS2Vector * vIn)
{
	// Multiply Vector with Matrix (apply transform).
	// This was salvaged from libVu0.c
	asm volatile (
		"lqc2         vf4, 0x0(%1)  \n\t"
		"lqc2         vf5, 0x10(%1) \n\t"
		"lqc2         vf6, 0x20(%1) \n\t"
		"lqc2         vf7, 0x30(%1) \n\t"
		"lqc2         vf8, 0x0(%2)  \n\t"
		"vmulax.xyzw  ACC, vf4, vf8 \n\t"
		"vmadday.xyzw ACC, vf5, vf8 \n\t"
		"vmaddaz.xyzw ACC, vf6, vf8 \n\t"
		"vmaddw.xyzw  vf9, vf7, vf8 \n\t"
		"sqc2         vf9, 0x0(%0)  \n\t"
		: : "r" (vOut), "r" (m), "r" (vIn)
	);
}

// ========================================================

// Scale constants use to map a vertex to GS rasterizer space:
static const float GS_RASTER_SCALE_X = 2048.0f;
static const float GS_RASTER_SCALE_Y = 2048.0f;
static const float GS_RASTER_SCALE_Z = float(0xFFFFFF) / 32.0f;

// Expand scale factors into a vector (W ignored):
static const float V_GS_SCALE[4] __attribute__((aligned(16))) =
{
	GS_RASTER_SCALE_X,
	GS_RASTER_SCALE_Y,
	GS_RASTER_SCALE_Z,
	1.0f
};

// ========================================================

static inline void scaleVert(PS2Vector * v, float q)
{
	// Multiply `v` by `q` and scale XYZ using the GS scale factors.
	asm volatile (
		"lqc2      vf4, 0x0(%0)  \n\t"
		"lqc2      vf5, 0x0(%1)  \n\t"
		"mfc1      $8,  %2       \n\t"
		"qmtc2     $8,  vf6      \n\t"
		"vmulx.xyz vf4, vf4, vf6 \n\t"
		"vmul.xyz  vf4, vf4, vf5 \n\t"
		"vadd.xyz  vf4, vf4, vf5 \n\t"
		"sqc2 vf4, 0x0(%0)       \n\t"
		: : "r" (v), "r" (V_GS_SCALE), "f" (q)
		: "$8"
	);
}

// ========================================================

static inline bool clipTriangle(const PS2Vector * v0, const PS2Vector * v1, const PS2Vector * v2)
{
	// This was salvaged from libVu0.c
	register int ret;
	asm volatile (
		"vsub.xyzw vf04, vf00, vf00 \n\t"
		".set push                  \n\t"
		".set mips3                 \n\t"
		"li %0, 0x4580000045800000  \n\t"
		".set pop                   \n\t"
		"lqc2     vf06, 0x0(%1)     \n\t"
		"lqc2     vf08, 0x0(%2)     \n\t"
		"lqc2     vf09, 0x0(%3)     \n\t"
		"qmtc2    %0,   vf07        \n\t"
		"ctc2     $0,   $vi16       \n\t"
		"vsub.xyw vf05, vf06, vf04  \n\t"
		"vsub.xy  vf05, vf07, vf06  \n\t"
		"vsub.xyw vf05, vf08, vf04  \n\t"
		"vsub.xy  vf05, vf07, vf08  \n\t"
		"vsub.xyw vf05, vf09, vf04  \n\t"
		"vsub.xy  vf05, vf07, vf09  \n\t"
		"vnop                       \n\t"
		"vnop                       \n\t"
		"vnop                       \n\t"
		"vnop                       \n\t"
		"vnop                       \n\t"
		"cfc2 %0, $vi16             \n\t"
		"andi %0, %0, 0xC0          \n\t"
		: "=r" (ret)
		: "r"  (v0), "r" (v1), "r" (v2)
	);
	return ret != 0;
}

// ========================================================

static inline void ftoi4XYZ(const PS2Vector * vIn, int * vOut)
{
	// To integer fixed point. Format used for vertex positions.
	asm volatile (
		"lqc2       vf4, 0x0(%1) \n\t"
		"vftoi4.xyz vf5, vf4     \n\t"
		"sqc2       vf5, 0x0(%0) \n\t"
		: : "r" (vOut), "r" (vIn)
	);
}

// ========================================================

static inline void ftoi0XYZW(const PS2Vector * vIn, int * vOut)
{
	// Color val to integer fixed point. Used for colors only.
	// Multiply XYZ by 128 and convert to integer FP.
	const float q = 128.0f;
	asm volatile (
		"lqc2        vf4, 0x0(%1)  \n\t"
		"mfc1        $8,  %2       \n\t"
		"qmtc2       $8,  vf6      \n\t"
		"vmulx.xyz   vf4, vf4, vf6 \n\t"
		"vftoi0.xyzw vf5, vf4      \n\t"
		"sqc2        vf5, 0x0(%0)  \n\t"
		: : "r" (vOut), "r" (vIn), "f" (q)
		: "$8"
	);
}

// ========================================================

static inline void emitVert(u64 * & packetPtr, PS2Vector & tPos, const float q, const DrawVertex & dv)
{
	// Convert vertex position to fixed-point:
	int tPosFixed[4] __attribute__((aligned(16)));
	ftoi4XYZ(&tPos, tPosFixed);

	// Transform color to GS format and convert to fixed-point:
	int tColorFixed[4] __attribute__((aligned(16)));
	ftoi0XYZW(&dv.color, tColorFixed);

	// Store fixed-point color:
	color_t gsColor;
	gsColor.r = (u8)(tColorFixed[0]);
	gsColor.g = (u8)(tColorFixed[1]);
	gsColor.b = (u8)(tColorFixed[2]);
	gsColor.a = (u8)(tColorFixed[3]);
	gsColor.q = q;

	// Store texture coords unfixed but with perspective scale:
	texel_t gsTexel;
	gsTexel.u = dv.texCoord.x * q;
	gsTexel.v = dv.texCoord.y * q;

	// Store fixed-point position:
	xyz_t gsPos;
	gsPos.x = (u16)(tPosFixed[0]);
	gsPos.y = (u16)(tPosFixed[1]);
	gsPos.z = (u32)(tPosFixed[2]);

	// Add to packet:
	*(packetPtr)++ = gsColor.rgbaq;
	*(packetPtr)++ = gsTexel.uv;
	*(packetPtr)++ = gsPos.xyz;
}

// ========================================================

static inline void inverseMatrix(PS2Matrix * mOut, const PS2Matrix * mIn)
{
	// Inverse of input matrix.
	// This was salvaged from libVu0.c
	asm volatile (
		"lq          $8,  0x00(%1) \n\t"
		"lq          $9,  0x10(%1) \n\t"
		"lq          $10, 0x20(%1) \n\t"
		"lqc2        vf4, 0x30(%1) \n\t"
		"vmove.xyzw  vf5, vf4      \n\t"
		"vsub.xyz    vf4, vf4, vf4 \n\t"
		"vmove.xyzw  vf9, vf4      \n\t"
		"qmfc2       $11, vf4      \n\t"
		"pextlw      $12, $9,  $8  \n\t"
		"pextuw      $13, $9,  $8  \n\t"
		"pextlw      $14, $11, $10 \n\t"
		"pextuw      $15, $11, $10 \n\t"
		"pcpyld      $8,  $14, $12 \n\t"
		"pcpyud      $9,  $12, $14 \n\t"
		"pcpyld      $10, $15, $13 \n\t"
		"qmtc2       $8,  vf6      \n\t"
		"qmtc2       $9,  vf7      \n\t"
		"qmtc2       $10, vf8      \n\t"
		"vmulax.xyz  ACC, vf6, vf5 \n\t"
		"vmadday.xyz ACC, vf7, vf5 \n\t"
		"vmaddz.xyz  vf4, vf8, vf5 \n\t"
		"vsub.xyz    vf4, vf9, vf4 \n\t"
		"sq          $8,  0x00(%0) \n\t"
		"sq          $9,  0x10(%0) \n\t"
		"sq          $10, 0x20(%0) \n\t"
		"sqc2        vf4, 0x30(%0) \n\t"
		: : "r" (mOut), "r" (mIn)
		: "$8", "$9", "$10", "$11", "$12", "$13", "$14", "$15"
	);
}


// TODO
#if 0
#define TRIANGLE_BACK_FACE_CULL(v0, v1, v2) \
if (cullBackFacingTriangle(&eyePosModelSpace, &(v0), &(v1), &(v2))) \
{ \
    continue; \
}
#endif
#define TRIANGLE_BACK_FACE_CULL(v0,v1,v2)

#define TRIANGLE_XFORM_SCR_CLIP(v0, v1, v2)  \
	applyXForm(&tPos0, &mvpMatrix, &(v0)); \
	applyXForm(&tPos1, &mvpMatrix, &(v1)); \
	applyXForm(&tPos2, &mvpMatrix, &(v2)); \
	const float q0 = 1.0f / tPos0.w; \
	scaleVert(&tPos0, q0); \
	const float q1 = 1.0f / tPos1.w; \
	scaleVert(&tPos1, q1); \
	const float q2 = 1.0f / tPos2.w; \
	scaleVert(&tPos2, q2); \
	if (clipTriangle(&tPos0, &tPos1, &tPos2)) \
	{ \
		continue; \
	}

// ========================================================

#define DRAW3D_PROLOGUE() \
	BEGIN_DMA_TAG(currentFrameQwPtr); \
	if (currentTex != nullptr) \
	{ \
		setTextureBufferSampling(); \
	} \
	u64 * /*restrict*/ packetPtr = (u64 *)(draw_prim_start(currentFrameQwPtr, 0, &primDesc, &primColor))

// ========================================================

#define DRAW3D_EPILOGUE() \
	/* Check if we're in middle of a qword and pad it if needed: */ \
	while ((u32)(packetPtr) % 16) \
	{ \
		*packetPtr++ = 0u; \
	} \
	currentFrameQwPtr = draw_prim_end((qword_t *)(packetPtr), 3, DRAW_STQ_REGLIST); \
	END_DMA_TAG(currentFrameQwPtr); \
	drawCount3d++

// ========================================================

#define XFORM_TRIANGLE(v0, v1, v2) \
	PS2Vector tPos0, tPos1, tPos2; \
	TRIANGLE_BACK_FACE_CULL((v0).position, (v1).position, (v2).position); \
	TRIANGLE_XFORM_SCR_CLIP((v0).position, (v1).position, (v2).position); \
	emitVert(packetPtr, tPos0, q0, (v0)); \
	emitVert(packetPtr, tPos1, q1, (v1)); \
	emitVert(packetPtr, tPos2, q2, (v2));

// ========================================================

//
// Hardware constants:
//
enum
{
	// 16 Kilobytes (16384 bytes).
	SCRATCH_PAD_SIZE_BYTES = 0x4000,

	// 1024 128bit quadwords.
	SCRATCH_PAD_SIZE_QWORDS = SCRATCH_PAD_SIZE_BYTES / sizeof(qword_t),

	// The Scratch Pad (SPR) R/W memory is at a fixed address.
	SCRATCH_PAD_ADDRESS = 0x70000000,

	// ORing a pointer with this mask sets it to Uncached Accelerated (UCAB) space.
	UCAB_MEM_MASK = 0x30000000
};

class __attribute__((aligned(64))) RenderPacket
{
public:

    enum Type
    {
        NORMAL, // EE Ram
        UCAB, // Uncached Accelerated Memory (UCAB)
        SPR // Scratch Pad Memory
    };

    RenderPacket()
    {
        qwordBuff = NULL;
        qwordCount = 0;
        type = NORMAL;
    }
    RenderPacket( u32 quadWords, Type packetType )
    {
        qwordBuff = NULL;
        qwordCount = 0;
        type = NORMAL;

        Init( quadWords, packetType );
    }

    ~RenderPacket()
    {
        if ( qwordBuff == NULL ) {
            return;
        }

        if ( type == NORMAL )
        {
            free( qwordBuff );
        }
        else if ( type == UCAB )
        {
            u32 ucabAddr = (u32)qwordBuff;
            ucabAddr ^= UCAB_MEM_MASK;

            qwordBuff = (qword_t*)ucabAddr;

            free( qwordBuff );
        }
        else
        {
            // Scratch pad memory isn't freed;
            // is a fixed hardware buffer
        }

        qwordBuff = NULL;
    }

    void Init( u32 quadWords, Type packetType )
    {
        type = packetType;

        // Scratchpad memory
        if ( type == SPR )
        {
            if ( quadWords > SCRATCH_PAD_SIZE_QWORDS )
            {
                printf( "RenderPacket::Init: scratch pad mem num quadWords too large: %u > %u\n",
                       quadWords, SCRATCH_PAD_SIZE_QWORDS );
            }
            else
            {
                qwordBuff = (qword_t*)SCRATCH_PAD_ADDRESS;
                qwordCount = SCRATCH_PAD_SIZE_QWORDS;
            }
        }
        // Heap
        else
        {
            qwordBuff = (qword_t*)memalign( 64, quadWords );
            if ( qwordBuff == NULL )
            {
                printf( "RenderPacket::Init: failed to allocate from heap\n" );
            }
            qwordCount = quadWords;
        }
        // mark the pointer attribute to UCAB space
        if ( type == UCAB )
        {
            u32 ucabAddr = (u32)qwordBuff;
            ucabAddr |= UCAB_MEM_MASK;

            qwordBuff = (qword_t*)ucabAddr;
        }
    }

    qword_t* GetQwordPtr() const { return qwordBuff; }

    u32 GetQWordCount() const { return qwordCount; }

    u32 GetDisplacement( const qword_t* q ) const
    {
        //printf("Displacement: %d\n", (u32)(q - qwordBuff));
        return (u32)(q - qwordBuff);
    }

private:

    RenderPacket(const RenderPacket&);
    RenderPacket& operator = (const RenderPacket&);

    qword_t* qwordBuff; // Aligned to a cache line (`memAlloc(align=64)`)
    u32 qwordCount; // Size of `qwordBuff` in qword_t's
    Type type;
};

class Renderer
{
public:

    Renderer() :
        currentFramePacket(NULL),
        currentFrameQwPtr(NULL),
        dmaTagDraw2d(NULL),
        currentTex(NULL),
        frameIndex(0),
        drawCount2d(0),
        drawCount3d(0),
        vramUserTextureStart(0),
        inMode2d(false),
        inMode3d(false)
    {
        fontManager.texAtlas = &texAtlas;
    }

    ~Renderer(){}

    bool Init(int width,
              int height,
              int vidMode,
              int fbPsm,
              int zPsm,
              bool interlaced)
    {
        // Reset the VRam pointer to the first address,
        graph_vram_clear();
        //gVramUsedBytes = 0;

        initGsBuffers(width, height, vidMode, fbPsm, zPsm, interlaced);
        initDrawingEnvironment();

        // create double buffer render packets
        //
        // FRAME_PACKET_SIZE is the number of quadwords per render packet
        // in the double buffer. 2 of them, so result is ~2MB memory
        //
        // No overflow checking is done - drawing large mesh could crash
        const u32 FRAME_PACKET_SIZE = 65535;
        framePackets[0].Init(FRAME_PACKET_SIZE, RenderPacket::NORMAL);
        framePackets[1].Init(FRAME_PACKET_SIZE, RenderPacket::NORMAL);

        // Extra packets for texture uploads
        textureUploadPacket[0].Init(128, RenderPacket::NORMAL);
        textureUploadPacket[1].Init(128, RenderPacket::NORMAL);

        // Small UCAB packet to send the flip buffer command
        flibFbPacket.Init(8, RenderPacket::UCAB);

        // Reset to be safe
        currentFramePacket = NULL;
        currentFrameQwPtr = NULL;
        currentTex = NULL;
        frameIndex = 0;

        // Init other aux data and default render states
        primDesc.type = PRIM_TRIANGLE;
        primDesc.shading = PRIM_SHADE_GOURAUD;
        primDesc.mapping = DRAW_ENABLE;
        primDesc.fogging = DRAW_DISABLE;
        primDesc.blending = DRAW_DISABLE;
        primDesc.antialiasing = DRAW_DISABLE;
        primDesc.mapping_type = PRIM_MAP_ST;
        primDesc.colorfix = PRIM_UNFIXED;

        primColor.r = 255;
        primColor.g = 255;
        primColor.b = 255;
        primColor.a = 255;
        primColor.q = 1.0f;

        screenColor.r = 0;
        screenColor.g = 0;
        screenColor.b = 0;
        screenColor.a = 255;
        screenColor.q = 1.0f;

        inMode2d = false;
        inMode3d = false;

        //memset(&fps, 0, sizeof(fps));

        modelMatrix.makeIdentity();
        invModelMatrix.makeIdentity();
        vpMatrix.makeIdentity();
        mvpMatrix.makeIdentity();

        eyePosition = PS2Vector(0.0f, 0.0f, 0.0f, 1.0f);
        eyePosModelSpace = eyePosition;

        // This only really affects 2D drawing. It simply sets a flag
        // inside the library, which gets tested by the 2D drawing routines
        // So we can set this once on initialization and forget
        draw_enable_blending();

        printf("Renderer::Init() complete!\n");
        return true;
    }

    void BeginFrame()
    {
        drawCount2d = 0;
        drawCount3d = 0;

        //printf("Frame Index: %d\n", frameIndex);
        currentFramePacket = &framePackets[frameIndex];
        currentFrameQwPtr = currentFramePacket->GetQwordPtr();
        //printf("Frame QWPtr: 0x%X\n", (u32)currentFrameQwPtr);
    }

    void EndFrame()
    {
        // add finish command to DMA chain
        BEGIN_DMA_TAG(currentFrameQwPtr);
        currentFrameQwPtr = draw_finish(currentFrameQwPtr);
        END_DMA_TAG_AND_CHAIN(currentFrameQwPtr);

        dma_wait_fast();
        dma_channel_send_chain(DMA_CHANNEL_GIF, currentFramePacket->GetQwordPtr(), currentFramePacket->GetDisplacement(currentFrameQwPtr), 0, 0);

        // V-Sync wait
        graph_wait_vsync();
        draw_wait_finish();

        graph_set_framebuffer_filtered(framebuffers[frameIndex].address, framebuffers[frameIndex].width, framebuffers[frameIndex].psm, 0, 0);

        // switch context
        // 1 XOR 1 = 0
        // 0 XOR 1 = 1
        frameIndex ^= 1;

        // swap render framebuffer
        flipBuffers(framebuffers[frameIndex]);
    }

    void Begin3D()
    {
        // assert(!inMode3d && "Already in 3D drawing mode!");
        inMode3d = true;
    }

    void End3D()
    {
        // assert(inMode3d && "Not in 3D drawing mode!");
        inMode3d = false;
    }

    void Begin2D(const Texture* tex = NULL)
    {
        // assert(inMode2d && "Not in 2D drawing mode!");
        inMode2d = true;

        //if (tex == NULL)
        //{
        //    printf("Setting Atlas texture\n");
        //}
        SetTexture((tex == NULL) ? texAtlas.GetTexture() : (*tex));

        BEGIN_DMA_TAG_NAMED(dmaTagDraw2d, currentFrameQwPtr);
        currentFrameQwPtr = draw_primitive_xyoffset(currentFrameQwPtr, 0, 2048, 2048);
        setTextureBufferSampling();
    }
    void End2D()
    {
        // assert(inMode2d && "Not in 2D drawing mode!");

        currentFrameQwPtr = draw_primitive_xyoffset(
            currentFrameQwPtr, 
            0, 
            2048 - (GetScreenWidth() / 2), 
            2048 - (GetScreenHeight() / 2));

        END_DMA_TAG_NAMED(dmaTagDraw2d, currentFrameQwPtr);
        inMode2d = false;
    }

    void ClearScreen()
    {
        BEGIN_DMA_TAG(currentFrameQwPtr);

        currentFrameQwPtr = draw_disable_tests(currentFrameQwPtr, 0, &zbuff);

        const uint width = GetScreenWidth();
        const uint height = GetScreenHeight();

        //printf("Screen width, height: %d, %d\n", width, height);

        currentFrameQwPtr = draw_clear(currentFrameQwPtr, 0, 2048 - (width / 2), 2048 - (height / 2), width, height, screenColor.r, screenColor.g, screenColor.b);
        currentFrameQwPtr = draw_enable_tests(currentFrameQwPtr, 0, &zbuff);

        END_DMA_TAG(currentFrameQwPtr);
    }

    // Draws a set of indexed vertexes that in turn compose a set
	// of triangle primitives. Index buffer and verts buffer must not overlap!
	void DrawIndexedTriangles(const u16 * indexes, u32 indexCount,
	                          const DrawVertex * verts, u32 vertCount)
    {
        if (indexCount % 3 != 0)
        {
            printf("DrawIndexedTris index count must be divisible by 3\n");
            return;
        }

        DRAW3D_PROLOGUE();

        //u32 trisSentToGs = 0;
        const u32 triCount = indexCount / 3;

        for (u32 t = 0; t < triCount; ++t)
        {
            const DrawVertex& v0 = verts[indexes[(t*3) + 0]];
            const DrawVertex& v1 = verts[indexes[(t*3) + 1]];
            const DrawVertex& v2 = verts[indexes[(t*3) + 2]];
            XFORM_TRIANGLE(v0, v1, v2);
            //++trisSentToGs;
        }

        DRAW3D_EPILOGUE();
        //trisCount3d += trisSentToGs;
	}

    void DrawRectFilled( const Rect4i& rect, const Color4b color)
    {
        //assert(inmode2d && "2D mode required drawrectfilled");

        rect_t rc;
        rc.v0.x = rect.x;
        rc.v0.y = rect.y;
        rc.v0.z = 0xFFFFFFFF;
        rc.v1.x = rect.width + rect.x;
        rc.v1.y = rect.height + rect.y;
        rc.v1.z = 0xFFFFFFFF;
        rc.color.r = color.r;
        rc.color.g = color.g;
        rc.color.b = color.b;
        rc.color.a = color.a;
        rc.color.q = 1.0f;

        currentFrameQwPtr = draw_rect_filled( currentFrameQwPtr, 0, &rc );
        drawCount2d++;
    }

    float DrawChar(Vec2f& pos, const Color4b color, const FontManager::BuiltInFontId fontId, const int c)
    {
        // assert(inMode2d && "2d mode required!");

        if (!fontManager.IsBuiltInFontLoaded(fontId))
        {
            if (!fontManager.LoadBuiltInFont(fontId))
            {
                return 0;
            }
        }

        // - Text is aligned to the left.
	    // - `pos.x` always incremented by glyph.with.
	    // - Spaces increment `pos.x` by `glyph.with`; Tabs increment it by `glyph.width * 4` (4 spaces).
	    // - New lines are not handled, since we don't know the initial line x to return to.
        float charWidth = 0.0f;
        switch(c)
        {
        case ' ':
            charWidth = fontManager.GetWhiteSpaceWidth(fontId);
            pos.x += charWidth;
            break;

        case '\t':
            charWidth = fontManager.GetWhiteSpaceWidth(fontId);
            pos.x += charWidth * 4.0f;
            break;

        default:
            if (isgraph(c))
            {
                const int charIndex = c - fontManager.builtInFonts[fontId].charStart;
                if (charIndex >= 0 && charIndex < BuiltInFontBitmap::MAX_GLYPHS)
                {
                    const BuiltInFontGlyph& glyph = fontManager.builtInFonts[fontId].glyphs[charIndex];

                    texrect_t rc;
                    rc.v0.x = pos.x;
                    rc.v0.y = pos.y;
                    rc.v0.z = 0xFFFFFFFF;
                    rc.t0.u = glyph.u0;
                    rc.t0.v = glyph.v0;
                    rc.v1.x = pos.x + (glyph.w * fontManager.globalTextScale);
                    rc.v1.y = pos.y + (glyph.h * fontManager.globalTextScale);
                    rc.v1.z = 0xFFFFFFFF;
                    rc.t1.u = glyph.u1;
                    rc.t1.v = glyph.v1;
                    rc.color.r = color.r;
                    rc.color.g = color.g;
                    rc.color.b = color.b;
                    rc.color.a = color.a;
                    rc.color.q = 1.0f;

                    currentFrameQwPtr = draw_rect_textured(currentFrameQwPtr, 0, &rc);
                    drawCount2d++;

                    charWidth = (float)glyph.w;
                    pos.x += charWidth * fontManager.globalTextScale;
                }
            }
            break;
        }

        return charWidth;
    }

    void DrawText(Vec2f& pos, const Color4b color, const FontManager::BuiltInFontId fontId, const char* str)
    {
        // assert(fontId >= 0 && fontId < FontManager::FONT_COUNT)
        // assert(str != NULL)

        // newline handled by setting X back to initial and adding glyph height to y
        const float initialX = pos.x;
        for (u32 c = 0; str[c] != '\0'; ++c)
        {
            switch (str[c])
            {
            case '\n':
                pos.x = initialX;
                pos.y += fontManager.builtInFonts[fontId].bmpHeight * fontManager.globalTextScale;
                break;

            case '\r':
                pos.x = initialX;
                break;

            default:
                DrawChar(pos, color, fontId, str[c]);
                break;
            }
        }
    }

    void GetTextLength(const char* str, const FontManager::BuiltInFontId fontId, Vec2f& advance)
    {
        // assert(str != NULL)
        if (!fontManager.IsBuiltInFontLoaded(fontId))
        {
            if (!fontManager.LoadBuiltInFont(fontId))
            {
                return;
            }
        }

        advance.x = 0.0f;
        advance.y = 0.0f;

        const float initialX = advance.x;
        for (u32 c = 0; str[c] != '\0'; c++)
        {
            switch (str[c])
            {
            case '\n':
                advance.x = initialX;
                advance.y += fontManager.builtInFonts[fontId].bmpHeight * fontManager.globalTextScale;
                break;

            case '\r':
                advance.x = initialX;
                break;

            case '\t':
                advance.x += fontManager.GetWhiteSpaceWidth(fontId) * 4; // 4 spaces per tab
                break;

            case ' ':
                advance.x += fontManager.GetWhiteSpaceWidth(fontId);
                break;

            default:
                if (isgraph(str[c]))
                {
                    const int charIndex = str[c] - fontManager.builtInFonts[fontId].charStart;
                    if (charIndex >= 0 && charIndex < BuiltInFontBitmap::MAX_GLYPHS)
                    {
                        const BuiltInFontGlyph& glyph = fontManager.builtInFonts[fontId].glyphs[charIndex];
                        advance.x += glyph.w * fontManager.globalTextScale;
                    }
                }
                break;
            }
        }
    }

    void SetClearScreenColor(u8 r, u8 g, u8 b)
    {
        screenColor.r = r;
        screenColor.g = g;
        screenColor.b = b;
    }

    void SetGlobalTextScale( const float scale )
    {
        fontManager.globalTextScale = scale;
    }

    float GetGlobalTextScale() const
    {
        return fontManager.globalTextScale;
    }

    bool LoadBuiltinFont(FontManager::BuiltInFontId fontId)
    {
        return fontManager.LoadBuiltInFont(fontId);
    }

    void SetPrimTextureMapping( const bool enable )
    {
        if ( enable )
        {
            primDesc.mapping = DRAW_ENABLE;
            primDesc.mapping_type = PRIM_MAP_ST;
        }
        else
        {
            primDesc.mapping = DRAW_DISABLE;
            primDesc.mapping_type = DRAW_DISABLE;
        }
    }

    void SetTexture( const Texture& texture )
    {
        // no need to change
        if ( &texture == currentTex )
        {
            //printf("Already current tex\n");
            return;
        }

        currentTex = (Texture*)&texture;
        if (currentTex->GetPixels() == NULL)
        {
            printf("ERROR - currenttex getpixels null\n");
            while(1)
            {}
        }
        if (currentTex->GetTexBuffer().address != (u32)vramUserTextureStart)
        {
            printf("Error - currenttex address != vramUserTextureStart\n");
            while(1);
        }
        // assert(currentTex->getPixels() != NULL);
        // assert(currentTex->getTexBuffer().address == uint(vramUserTextureStart));

        flushPipeline();
        //texSwitches++;

        // upload to GS ram
        const u32 width = currentTex->GetWidth();
        const u32 height = currentTex->GetHeight();
        const u32 psm = currentTex->GetPixelFormat();
        u8* pixels = (u8*)currentTex->GetPixels();

        qword_t* q = textureUploadPacket[frameIndex].GetQwordPtr();
        q = draw_texture_transfer( q, 
                                   pixels, 
                                   width, 
                                   height, 
                                   psm, 
                                   vramUserTextureStart, 
                                   width );
        q = draw_texture_flush( q );
        dma_channel_send_chain( DMA_CHANNEL_GIF, 
                                textureUploadPacket[frameIndex].GetQwordPtr(),
                                textureUploadPacket[frameIndex].GetDisplacement(q), 
                                0, 
                                0 );
        dma_wait_fast();
    }

    void SetPrimAntialiasing( const bool enable )
    {
        primDesc.antialiasing = enable ? DRAW_ENABLE : DRAW_DISABLE;
    }

    void SetPrimShading( const int shading )
    {
        primDesc.shading = shading;
    }

    void SetViewProjMatrix(const PS2Matrix & vp)
    {
        vpMatrix = vp;
    }

    void SetModelMatrix(const PS2Matrix& m)
    {
        modelMatrix = m;
        mvpMatrix   = modelMatrix * vpMatrix;

        inverseMatrix(&invModelMatrix, &modelMatrix);
        applyXForm(&eyePosModelSpace, &invModelMatrix, &eyePosition);
    }

    float GetAspectRatio()
    {
        //return framebuffers[0].width / framebuffers[0].height;
        return 4.0f/3.0f;
    }

    u32 GetScreenWidth()
    {
        return framebuffers[0].width;
    }

    u32 GetScreenHeight()
    {
        return framebuffers[0].height;
    }

    u32 GetVRamUserTextureStart() const
    {
        return vramUserTextureStart;
    }

private:
    // Two packets/buffers for double buffering
    zbuffer_t zbuff;
    framebuffer_t framebuffers[2];
    RenderPacket framePackets[2];
    RenderPacket flibFbPacket;
    RenderPacket* currentFramePacket;
    qword_t* currentFrameQwPtr;
    qword_t* dmaTagDraw2d;
    Texture* currentTex;
    TextureAtlas texAtlas;
    FontManager fontManager;
    u32 frameIndex;
	u32 drawCount2d; // Number of 2D draw calls
	u32 drawCount3d; // Number of 3D draw calls

    // Texture mapping aux data
    RenderPacket textureUploadPacket[2];
    u32 vramUserTextureStart;

    // Current render matrices for 3D geometry transformation:
	PS2Matrix modelMatrix;
	PS2Matrix invModelMatrix;
	PS2Matrix vpMatrix;
	PS2Matrix mvpMatrix;
	PS2Vector eyePosition;
	PS2Vector eyePosModelSpace;

    // 3D Primitive/geometry attributes
    prim_t primDesc;
    color_t primColor;
    color_t screenColor;

    // 2D Rendering, including text, can only take place between `begin2d()` and `end2d()` calls
    bool inMode2d;

    // 3D rendering can only take place between `begin3d()` and `end3d()` calls
    bool inMode3d;

    inline int vramAlloc(int width,
                         int height,
                         int psm,
                         int alignment)
    {
        const int addr = graph_vram_allocate(width, height, psm, alignment);
        if (addr < 0)
        {
            printf("Failed to allocate VRam space!\n");
        }

        return addr;
    }

    void initGsBuffers(int width,
                       int height,
                       int vidMode,
                       int fbPsm,
                       int zPsm,
                       bool interlaced)
    {
        // init GIF DMA channel
        dma_channel_initialize(DMA_CHANNEL_GIF, NULL, 0);
        dma_channel_fast_waits(DMA_CHANNEL_GIF);

        // frame buffer 0
        framebuffers[0].width = width;
        framebuffers[0].height = height;
        framebuffers[0].mask = 0;
        framebuffers[0].psm = fbPsm;
        framebuffers[0].address =
            vramAlloc(width, height, fbPsm, GRAPH_ALIGN_PAGE);

        // frame buffer 1
        framebuffers[1].width = width;
        framebuffers[1].height = height;
        framebuffers[1].mask = 0;
        framebuffers[1].psm = fbPsm;
        framebuffers[1].address =
            vramAlloc(width, height, fbPsm, GRAPH_ALIGN_PAGE);

        // Z/Depth buffer
        zbuff.enable = DRAW_ENABLE;
        zbuff.mask = 0;
        zbuff.method = ZTEST_METHOD_GREATER_EQUAL;
        zbuff.zsm = zPsm;
        zbuff.address =
            vramAlloc(width, height, zPsm, GRAPH_ALIGN_PAGE);

        // User textures start after the z-buffer
        // Allocate space for a single 256x256 large texture
        //  (pretty much all the space we have left)
        vramUserTextureStart = vramAlloc(Texture::MAX_SIZE, Texture::MAX_SIZE, GS_PSM_32, GRAPH_ALIGN_BLOCK);
        Texture::vramUserTextureStart = vramUserTextureStart;

        // initialize the screen and tie the first framebuffer to the read circuits

        if (vidMode == GRAPH_MODE_AUTO)
        {
            vidMode = graph_get_region();
        }

        // set video mode with flicker filter
        const int graphMode = interlaced ?
            GRAPH_MODE_INTERLACED :
            GRAPH_MODE_NONINTERLACED;
        graph_set_mode(graphMode, vidMode, GRAPH_MODE_FIELD, GRAPH_ENABLE);

        graph_set_screen(0, 0, width, height);
        graph_set_bgcolor(0, 0, 0);
        graph_set_framebuffer_filtered(framebuffers[0].address, framebuffers[0].width, framebuffers[0].psm, 0, 0);
        graph_enable_output();
    }

    void initDrawingEnvironment()
    {
        RenderPacket packet(50, RenderPacket::NORMAL);

        // set framebuffer and virtual screen offsets
        qword_t* q = packet.GetQwordPtr();
        q = draw_setup_environment(q, 0, &framebuffers[0], &zbuff);
        q = draw_primitive_xyoffset(q,
                                    0,
                                    2048 - (GetScreenWidth() / 2),
                                    2048 - (GetScreenHeight() / 2));


        // texture wrapping mode fixed to REPEAT
        texwrap_t wrap;
        wrap.horizontal = WRAP_REPEAT;
        wrap.vertical = WRAP_REPEAT;
        wrap.minu = wrap.maxu = 0;
        wrap.minv = wrap.maxv = 0;
        q = draw_texture_wrapping(q, 0, &wrap);

        q = draw_finish(q);
        dma_channel_send_normal(DMA_CHANNEL_GIF, packet.GetQwordPtr(), packet.GetDisplacement(q), 0, 0);
        dma_wait_fast();
    }

    void flipBuffers(framebuffer_t& fb)
    {
        qword_t* q = flibFbPacket.GetQwordPtr();

        q = draw_framebuffer(q, 0, &fb);
        q = draw_finish(q);

        dma_wait_fast();
        dma_channel_send_normal_ucab(DMA_CHANNEL_GIF, flibFbPacket.GetQwordPtr(), flibFbPacket.GetDisplacement(q), 0);
        draw_wait_finish();
    }

    void flushPipeline()
    {
        if ( drawCount2d + drawCount3d == 0 )
        {
            return; // nothing to draw
        }

        // Add a finish command to the DMA chain
        BEGIN_DMA_TAG( currentFrameQwPtr );
        currentFrameQwPtr = draw_finish( currentFrameQwPtr );
        END_DMA_TAG_AND_CHAIN( currentFrameQwPtr );

        dma_channel_send_chain( DMA_CHANNEL_GIF, 
                                currentFramePacket->GetQwordPtr(),
                                currentFramePacket->GetDisplacement( currentFrameQwPtr ),
                                0, 0 );
        dma_wait_fast();

        // reset packet data pointer to next
        currentFramePacket = &framePackets[frameIndex];
        currentFrameQwPtr = currentFramePacket->GetQwordPtr();
    }

    void setTextureBufferSampling()
    {
        // assert(currentTex != NULL);
        // assert(currentFrameQwPtr != NULL)
        // assert(currentTex->GetTexBuffer().address == u32(vramUserTextureStart));

        currentFrameQwPtr = draw_texture_sampling( currentFrameQwPtr, 
                                                   0, 
                                                   &currentTex->GetTexLod() );
        currentFrameQwPtr = draw_texturebuffer( currentFrameQwPtr, 
                                                0, 
                                                &currentTex->GetTexBuffer(), 
                                                &currentTex->GetTexClut() );
    }
};

#endif // RENDERER_H_INCLUDED

