#ifndef INGAME_CONSOLE_H
#define INGAME_CONSOLE_H

// PS2DEV SDK:
#include <draw.h>
#include <graph.h>
#include <gs_psm.h>

#include <FontManager.h>
#include <Renderer.h>

//class PS2Renderer;
//class Renderer;
class IngameConsole
{
public:

    static const Color4b DEFAULT_TEXT_COLOR;
    static const Color4b BACKGROUND_COLOR;
    static const u32     TEX_CHAR_HEIGHT = 24;
    static const float   TEXT_START_OFFSET = 10.0f;
    static const FontManager::BuiltInFontId TEXT_CHAR_FONT = /*FontManager::FONT_CONSOLAS_24*/FontManager::FONT_BJ_TEST;

    IngameConsole() :
        currentTextColor(DEFAULT_TEXT_COLOR),
        currentLine(&lines[0]),
        linesUsed(1)
    {
        memset(lines, 0, sizeof(lines));
    }

    void SetRenderer(Renderer* render)
    {
        this->pRenderer = render;
    }

    void Print(const char* str)
    {
        //assert(currentLine != NULL);

        if (str == NULL || *str == '\0')
        {
            return;
        }

        for (Line* line = currentLine; *str != '\0'; ++str)
        {
            if (*str == '\n' || line->charsUsed == MAX_CHARS_PER_LINE)
            {
                line = allocNewLine();
                // skip newline chars
                if (*str == '\n')
                {
                    continue;
                }
            }

            //assert(line->charsUsed < MAX_CHARS_PER_LINE);
            Char& lineChar = line->chars[line->charsUsed++];
            lineChar.chr = *str;
            lineChar.r = currentTextColor.r;
            lineChar.g = currentTextColor.g;
            lineChar.b = currentTextColor.b;
        }
    }

    void Draw() const
    {
        if (linesUsed <= 1 && lines[0].charsUsed == 0)
        {   
            return;
        }

        // console window background
        const Rect4i rect = {
            0, 0,
            pRenderer->GetScreenWidth(),
            (int)((float)getHeight() * pRenderer->GetGlobalTextScale()),
        };
        pRenderer->DrawRectFilled(rect, BACKGROUND_COLOR);

        // text
        Vec2f pos = { TEXT_START_OFFSET, TEXT_START_OFFSET };
        for (u32 l = 0; l < linesUsed; l++)
        {
            printf("Console print: ");
            for (u32 c = 0; c < lines[l].charsUsed; c++)
            {
                Color4b color;
                color.r = lines[l].chars[c].r;
                color.g = lines[l].chars[c].g;
                color.b = lines[l].chars[c].b;
                color.a = 255;
                printf("%c, ", lines[l].chars[c].chr);
                pRenderer->DrawChar(pos, color, TEXT_CHAR_FONT, lines[l].chars[c].chr);
            }
            printf("\n");

            pos.x = TEXT_START_OFFSET;
            pRenderer->DrawText(pos, DEFAULT_TEXT_COLOR, TEXT_CHAR_FONT, "\n");
        }
    }

    void Clear()
    {
        currentTextColor = DEFAULT_TEXT_COLOR;
        currentLine = &lines[0];
        linesUsed = 1;

        memset(lines, 0, sizeof(lines));
    }

    void SetTextColor(Color4b color)
    {
        currentTextColor = color;
    }

    void SetTextColor(u32 color)
    {
        currentTextColor.r = (color & 0xFF000000) >> 24;
        currentTextColor.g = (color & 0x00FF0000) >> 16;
        currentTextColor.b = (color & 0x0000FF00) >> 8;
        currentTextColor.a = (color & 0x000000FF);
    }

    void RestoreTextColor()
    {
        currentTextColor = DEFAULT_TEXT_COLOR;
    }

    void PreloadFonts()
    {
        //if (!pRenderer->IsBuiltinFontLoaded(TEXT_CHAR_FONT))
        //{
        //    pRenderer->LoadBuiltinFont(TEXT_CHAR_FONT);
        //}
        pRenderer->LoadBuiltinFont(TEXT_CHAR_FONT);
    }

private:

    IngameConsole(const IngameConsole& );
    IngameConsole& operator=(const IngameConsole& );

    static const u32 MAX_TEXT_LINES = 20;
    static const u32 MAX_CHARS_PER_LINE = 80;

    Renderer* pRenderer;

    // Letter plus its color
    struct __attribute__((aligned(16))) Char
    {
        u8 chr;
        u8 r;
        u8 g;
        u8 b;
    };

    // Fixed-length array of characters
    struct __attribute__((aligned(16))) Line
    {
        u32 charsUsed;
        Char chars[MAX_CHARS_PER_LINE];
    };

    // possibly gets rid of the oldest line
    Line* allocNewLine()
    {
        if (linesUsed == MAX_TEXT_LINES)
        {
            // remove 0 (the oldest), and shift the rest
            linesUsed--;
            memmove(&lines[0], &lines[1], linesUsed * sizeof(Line));
        }

        currentLine = &lines[linesUsed++];
        currentLine->charsUsed = 0;
        return currentLine;
    }

    const u32 getHeight() const
    {
        if (linesUsed <= 1 && lines[0].charsUsed == 0)
        {
            return 0;
        }
        return (linesUsed + 1) * TEX_CHAR_HEIGHT;
    }

    Color4b currentTextColor;
    Line* currentLine;

    u32 linesUsed;
    Line lines[MAX_TEXT_LINES];
};

const Color4b IngameConsole::DEFAULT_TEXT_COLOR = { 255, 255, 255, 255 };
const Color4b IngameConsole::BACKGROUND_COLOR = { 10, 70, 40, 75 };

#endif // INGAME_CONSOLE_H

