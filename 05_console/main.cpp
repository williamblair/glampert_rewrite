#include <kernel.h>
#include <tamtypes.h>
#include <climits>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <malloc.h>
#include <sifrpc.h>
#include <loadfile.h>
#include <libpad.h>
#include <stdio.h>
#include <draw.h>
#include <graph.h>
#include <gs_psm.h>
#include <cctype>
#include <math.h>
#include <dma.h>
#include <dma_tags.h>
#include <gif_tags.h>
#include <gs_gp.h>
//#include <vertex_xform.h>

#include <PS2Vector.h>
#include <PS2Matrix.h>
#include <PS2MathFuncs.h>

//#include <Renderer.h>
#include "Texture.h"
#include "TextureAtlas.h"
#include <IngameConsole.h>


// TODO - move this into CPP file or something
u32 Texture::vramUserTextureStart = 0;

static inline double deg2rad( const double deg )
{
    return deg * (3.141592653589793f / 180.0f);
}

// ========================================================

struct Cube
{
	static const u32 VERT_COUNT  = 24;
	static const u32 INDEX_COUNT = 36;

	DrawVertex * vertexes;
	u16     * indexes;
};

// ========================================================

Cube MakeCube(const PS2Vector & color, const float scale)
{
	// Face indexes:
	static const u16 cubeF[6][4] =
	{
		{ 0, 1, 5, 4 }, { 4, 5, 6, 7 }, { 7, 6, 2, 3 },
		{ 1, 0, 3, 2 }, { 1, 2, 6, 5 }, { 0, 4, 7, 3 }
	};

	// Positions/vertexes:
	static const float cubeV[8][3] =
	{
		{ -0.5f, -0.5f, -0.5f },
		{ -0.5f, -0.5f,  0.5f },
		{  0.5f, -0.5f,  0.5f },
		{  0.5f, -0.5f, -0.5f },

		{ -0.5f,  0.5f, -0.5f },
		{ -0.5f,  0.5f,  0.5f },
		{  0.5f,  0.5f,  0.5f },
		{  0.5f,  0.5f, -0.5f },
	};

	// Tex coords:
	static const float cubeT[4][2] =
	{
		{ 0.0f, 0.0f },
		{ 0.0f, 1.0f },
		{ 1.0f, 1.0f },
		{ 1.0f, 0.0f }
	};

	// Allocate a new data for a new cube:
	Cube cube;
	//cube.vertexes = new(MEM_TAG_GEOMETRY) DrawVertex[Cube::VERT_COUNT];
	cube.vertexes = (DrawVertex*)memalign(16, Cube::VERT_COUNT*sizeof(DrawVertex));
	//cube.indexes  = new(MEM_TAG_GEOMETRY) uint16[Cube::INDEX_COUNT];
	cube.indexes = (u16*)memalign(16, Cube::INDEX_COUNT*sizeof(u16));

	// Fill in the data:
    DrawVertex * vertexPtr = cube.vertexes;
    u16     * facePtr   = cube.indexes;

	// `i` iterates over the faces, 2 triangles per face:
	u16 vertIndex = 0;
	for (u32 i = 0; i < 6; ++i, vertIndex += 4)
	{
		for (u32 j = 0; j < 4; ++j)
		{
			vertexPtr->position.x = cubeV[cubeF[i][j]][0] * scale;
			vertexPtr->position.y = cubeV[cubeF[i][j]][1] * scale;
			vertexPtr->position.z = cubeV[cubeF[i][j]][2] * scale;
			vertexPtr->position.w = 1.0f;

			vertexPtr->texCoord.x = cubeT[j][0];
			vertexPtr->texCoord.y = cubeT[j][1];
			vertexPtr->texCoord.z = 0.0f;
			vertexPtr->texCoord.w = 1.0f;

			vertexPtr->color = color;
			++vertexPtr;
		}

		facePtr[0] = vertIndex;
		facePtr[1] = vertIndex + 1;
		facePtr[2] = vertIndex + 2;
		facePtr += 3;

		facePtr[0] = vertIndex + 2;
		facePtr[1] = vertIndex + 3;
		facePtr[2] = vertIndex;
		facePtr += 3;
	}

	return cube;
}

void MakeCheckerTexture(Texture& texture)
{
    const int NUM_SQUARES = 8;
    const int IMG_SIZE = 256; // width = height = 256
    const int CHECKER_SIZE = IMG_SIZE / NUM_SQUARES;
    static Color4b data[IMG_SIZE * IMG_SIZE] __attribute__((aligned(16)));

    const Color4b pattern[] = 
    {
        {0,   0,   0,   255},
        {255, 255, 255, 255}
    };

    int startY = 0;
    int lastColor = 0;
    int color, rowX;

    while (startY < IMG_SIZE)
    {
        for (int y = startY; y < (startY + CHECKER_SIZE); y++)
        {
            color = lastColor;
            rowX = 0;
            for (int x = 0; x < IMG_SIZE; x++)
            {
                if (rowX == CHECKER_SIZE)
                {
                    color = !color;
                    rowX = 0;
                }
                data[x + y * IMG_SIZE] = pattern[color];
                rowX++;
            }
        }
        startY += CHECKER_SIZE;
        lastColor = !lastColor;
    }

    // non-mipmapped texture
    lod_t lod;
    lod.calculation = LOD_USE_K;
    lod.max_level = 0;
    lod.mag_filter = LOD_MAG_LINEAR;
    lod.min_filter = LOD_MIN_LINEAR;
    lod.l = 0;
    lod.k = 0.0f;
    if (!texture.InitFromMemory((u8*)data, 
                                TEXTURE_COMPONENTS_RGBA, 
                                IMG_SIZE, IMG_SIZE, 
                                GS_PSM_32, 
                                TEXTURE_FUNCTION_MODULATE, 
                                &lod))
    {
        printf("Failed to load checker texture\n");
    }
}

Renderer renderer;
PS2Matrix modelMatrix;
PS2Matrix  gViewMatrix;
PS2Matrix  gProjectionMatrix;

Texture gTexture;
Cube gRedCube;
Cube gWhiteCube;
Cube gGreenCube;
float gCubeRotation = 0.0f;

IngameConsole console;

static inline void DrawCubeGroup(const Cube & cube, const float y)
{
	for (u32 z = 0; z < 3; ++z)
	{
		for (u32 x = 0; x < 3; ++x)
		{
			PS2Matrix trans, rotX, rotY;
			trans.makeTranslation((x * 5.0f) - 5.0f, y, (z * -5.0f) - 20.0f);
			rotX.makeRotationX(deg2rad(gCubeRotation));
			rotY.makeRotationY(deg2rad(gCubeRotation));

			renderer.SetModelMatrix(rotX * rotY * trans);
			renderer.DrawIndexedTriangles(cube.indexes, Cube::INDEX_COUNT, cube.vertexes, Cube::VERT_COUNT);
		}
	}
}

static void DrawCubes()
{
    //gViewMatrix = gCamera.getViewMatrix();
	renderer.SetViewProjMatrix(gViewMatrix * gProjectionMatrix);

	gCubeRotation += 0.3f;
	if (gCubeRotation >= 360.0f)
	{
		gCubeRotation = 0.0f;
	}

	renderer.SetTexture( gTexture );
	DrawCubeGroup( gRedCube,  -5.0f );
	DrawCubeGroup( gWhiteCube, 0.0f );
	DrawCubeGroup( gGreenCube, 5.0f );
}

int main(int argc, char* argv[])
{
    if (!renderer.Init( 640, 448,         // 640x448 is standard NTSC resolution
                   GRAPH_MODE_AUTO,  // Select best mode (PAL or NTSC)
                   GS_PSM_32,        // Framebuffer color format
                   GS_PSMZ_32,       // Z-buffer format
                   true ))            // Interlace ON
    {
        printf("Failed to init graphics\n");
        while (1)
        {
        }
    }

    printf("End renderer init\n");

    renderer.SetClearScreenColor( 65, 25, 0 );
    renderer.SetGlobalTextScale( 0.67f );
    renderer.SetPrimTextureMapping( true );
    renderer.SetPrimAntialiasing( true );
    renderer.SetPrimShading( PRIM_SHADE_FLAT );

	console.SetRenderer( &renderer );
	console.PreloadFonts();

    gViewMatrix.makeIdentity();
    gProjectionMatrix.makePerspectiveProjection(deg2rad(60.0f), renderer.GetAspectRatio(), renderer.GetScreenWidth(), renderer.GetScreenHeight(), 2.0f, 1000.0f);
    modelMatrix.makeIdentity();

    // Create a few cubes:
	//
	gRedCube   = MakeCube(PS2Vector(1.0f, 0.0f, 0.0f, 1.0f), 2.0f);
	gWhiteCube = MakeCube(PS2Vector(1.0f, 1.0f, 1.0f, 1.0f), 2.0f);
	gGreenCube = MakeCube(PS2Vector(0.0f, 1.0f, 0.0f, 1.0f), 2.0f);

    // Generate and load a texture
    MakeCheckerTexture( gTexture );

	static Rect4i rect;
	static Color4b color;
	console.Print("==== Welcome to the Cubes demo! ====");
    for (;;)
    {
        //printf("Begin frame\n");
        renderer.BeginFrame();
        renderer.ClearScreen();

		renderer.Begin3D();
		{
            DrawCubes();
		}
		renderer.End3D();

		//printf("Begin 2d\n");
		renderer.Begin2D();
		{
			Vec2f pos = { 5.0f, 5.0f };
			Color4b textCol = { 255, 255, 255, 255 };
			renderer.DrawText(pos, textCol, FontManager::FONT_CONSOLAS_24, "Pre");
			//console.Draw();

			rect.x = 10;
			rect.y = 50;
			rect.width = 100;
			rect.height = 100;
			color.r = 255;
			color.b = 0;
			color.g = 0;
			color.a = 255;
			//printf("Draw rect filled\n");
			renderer.DrawRectFilled( rect, color );
		}
		//printf("End 2d\n");
		renderer.End2D();

        //printf("End frame\n");
        renderer.EndFrame();
        //printf("end loop\n");
    }

    SleepThread();

    return 0;
}
