#ifndef TEXTURE_H_INCLUDED
#define TEXTURE_H_INCLUDED

#include <array.hpp>
#include <ImageData.h>
//#include <TextureAtlas.h>

// PS2DEV SDK:
#include <draw.h>
#include <graph.h>
#include <gs_psm.h>

// STBI image implementation
namespace
{
#ifndef STB_IMAGE_IMPLEMENTATION
#define STB_IMAGE_IMPLEMENTATION

// Only need these for now:
#define STBI_ONLY_JPEG
#define STBI_ONLY_PNG
#define STBI_ONLY_TGA

// Don't need HDR nor loading from file.
#define STBI_NO_HDR
#define STBI_NO_LINEAR
#define STBI_NO_STDIO
#define STBI_NO_SIMD

// Use our custom assert:
//#define STBI_ASSERT ps2assert

// Use 128-aligned allocations.
// This will force STBI to use 128bytes aligned allocations for fresh allocs:
//#define STBI_MALLOC(size)          memTagMalloc(MEM_TAG_TEXTURE, (size), 128)
#define STBI_MALLOC(size)          memalign(128, (size))
//#define STBI_REALLOC(ptr, newSize) memTagRealloc(MEM_TAG_TEXTURE, (ptr), (newSize))
#define STBI_REALLOC(ptr, newSize) realloc((ptr), (newSize))
//#define STBI_FREE(ptr)             memTagFree(MEM_TAG_TEXTURE, (ptr))
#define STBI_FREE(ptr)             free((ptr))

// STBI header-only library:
#include "../stb_image.h"

#endif
}

// Loads a JPG, TGA or PNG from a memory buffer with the file contents.
// If `forceRgba` is true, output `image.comps` will always be 4.
static inline bool loadImageFromMemory(const u8* data, uint sizeBytes, ImageData& image, bool forceRgba)
{
    memset(&image, 0, sizeof(image));
    
    if (data == NULL || sizeBytes == 0)
    {
        printf("Invalid data buffer\n");
        return false;
    }

    int w, h, c;
    stbi_uc* pixels = stbi_load_from_memory(data, sizeBytes, &w, &h, &c, (forceRgba ? 4 : 0));

    if (pixels == NULL)
    {
        printf("stbi load failed, error: %s\n", stbi_failure_reason());
        return false;
    }

    // check power of 2 dimensions
    if ((w & (w - 1) != 0) || (h & (h - 1) != 0))
    {
        printf("WARNING: image not power of 2 dims");
    }

    image.pixels = pixels;
    image.width = w;
    image.height = h;
    image.comps = (forceRgba ? 4 : c);

    printf("Loaded image from memory: (w,h,components): %d,%d,%d\n", w, h, c);

    return true;
}

// Frees image data. Should be called on every ImageData instance when it gets disposed.
static inline void imageCleanup(ImageData& image)
{
    if (image.pixels != NULL)
    {
        stbi_image_free(image.pixels);
    }

    memset(&image, 0, sizeof(image));
}

class Texture
{
public:

    // max square size is 256x256x4 (4 is RGBA)
    static const u32 MAX_SIZE = 256;
    static u32 vramUserTextureStart;

    // constructor
    Texture() :
        texData(NULL),
        texHeight(0),
        texBuf(),
        texLod(),
        texClut()
    {}

    bool InitEmpty(u8 comps,
                   u32 w,
                   u32 h,
                   u32 psm,
                   u8 func,
                   const lod_t* lod = NULL,
                   const clutbuffer_t* clut = NULL)
    {
        // TODO
        //if (w == 0) {ERROR}
        //if (h == 0) {ERROR}

        if (w > MAX_SIZE) {
            printf("Texture width greater than max %d: %d\n", MAX_SIZE, w);
        }
        if (h > MAX_SIZE) {
            printf("Texture height greater than max %d: %d\n", MAX_SIZE, h);
        }

        texHeight = h;
        texBuf.width = w;
        texBuf.psm = psm;
        texBuf.info.width = draw_log2(w);
        texBuf.info.height = draw_log2(h);
        texBuf.info.components = comps;
        texBuf.info.function = func;
        
        // All textures must share the same VRam space. Only enough VRam left
        // for a single texture at a time. Every texture must be copied into
        // vram on the fly before use
        //texBuf.address = gRenderer.getVRamUserTextureStart();
        texBuf.address = vramUserTextureStart;

        if (lod != NULL)
        {
            texLod = *lod;
        }
        else
        {
            // defaults
            texLod.calculation = LOD_USE_K;
            texLod.max_level = 0;
            texLod.mag_filter = LOD_MAG_NEAREST;
            texLod.min_filter = LOD_MIN_NEAREST;
            texLod.l = 0;
            texLod.k = 0.0f;
        }

        if (clut != NULL)
        {
            texClut = *clut;
        }
        else
        {
            // defaults
            texClut.address = 0;
            texClut.psm = 0;
            texClut.storage_mode = CLUT_STORAGE_MODE1;
            texClut.start = 0;
            texClut.load_method = CLUT_NO_LOAD;
        }

        return true;
    }

    // Init from existing
    bool InitFromMemory(const u8* data,
                        u8 comps,
                        u32 width,
                        u32 height,
                        u32 psm,
                        u8 func,
                        const lod_t* lod = NULL,
                        const clutbuffer_t* clut = NULL)
    {
        if (!InitEmpty(comps, width, height, psm, func, lod, clut))
        {
            return false;
        }

        texData = data;
        return true;
    }
    
    static void LoadPermanentTexture(Texture& texture, 
                                     const u8* data, 
                                     const u32 sizeBytes, 
                                     const u8 textureFunction)
    {
        ImageData img;
        if (!loadImageFromMemory(data, 
                                 sizeBytes, 
                                 img, 
                                 true)) // true = force rgba
        {
            printf("Failed to load texture from location %p\n", data);
            while (1);
        }
        // image should be r,g,b,a
        if (img.comps != 4)
        {
            printf("Converted image should be RGBA, but comps != 4\n");
            while (1);
        }
    
        // Texture isn't mipmapped
        lod_t lod;
        lod.calculation = LOD_USE_K;
        lod.max_level = 0;
        lod.mag_filter = LOD_MAG_LINEAR;
        lod.min_filter = LOD_MIN_LINEAR;
        lod.l = 0;
        lod.k = 0.0f;
        if (!texture.InitFromMemory(img.pixels, 
                                    TEXTURE_COMPONENTS_RGBA, 
                                    img.width, img.height, 
                                    GS_PSM_32, 
                                    textureFunction, 
                                    &lod))
        {
            printf("Failed to init image from memory!\n");
            while (1);
        }
        printf("Successfully loaded permanent texture\n");
    }

    const u8* GetPixels() const
    {
        return texData;
    }
    void SetPixels(const u8* pixels)
    {
        texData = pixels;
    }

    texbuffer_t& GetTexBuffer() { return texBuf; }
    clutbuffer_t& GetTexClut() { return texClut; }
    lod_t& GetTexLod() { return texLod; }

    u32 GetWidth() const
    {
        return texBuf.width;
    }
    u32 GetHeight() const
    {
        return texHeight;
    }
    u32 GetPixelFormat() const
    {
        return texBuf.psm;
    }

private:

    // no copy
    Texture(const Texture&);
    Texture& operator = (const Texture&);

    const u8* texData; // external data ptr
    u32 texHeight;
    texbuffer_t texBuf;
    lod_t texLod;
    clutbuffer_t texClut;
};

#endif // TEXTURE_H_INCLUDED

