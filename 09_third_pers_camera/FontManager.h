#ifndef FONTMGR_H_INCLUDED
#define FONTMGR_H_INCLUDED

// "Consolas" fonts (see `builtin_fonts/` dir):
#include "builtin_fonts/consolas10.h"
#include "builtin_fonts/consolas12.h"
#include "builtin_fonts/consolas16.h"
#include "builtin_fonts/consolas20.h"
#include "builtin_fonts/consolas24.h"
#include "builtin_fonts/consolas30.h"
#include "builtin_fonts/consolas36.h"
#include "builtin_fonts/consolas40.h"
#include "builtin_fonts/consolas48.h"
#include "builtin_fonts/font_0.c"

struct __attribute__((aligned(16))) BuiltInFontGlyph
{
    // Texture coordinates to use when rendering, in pixels
    s32 u0, v0;
    s32 u1, v1;

    // glyph bitmap size in pixels
    s32 w, h;

    // point to start of glyph bitmap data
    // e.g. builtInFontBitmap->bitmap[]
    u32* pixels;
};

struct __attribute__((aligned(16))) BuiltInFontBitmap
{
    // number of glyphs in use and frst char
    s32 numGlyphs;
    s32 charStart;

    // dimensions in pixels
    s32 bmpWidth;
    s32 bmpHeight;

    // Bitmap allocated to store decompressed glyphs.
	// Bitmap glyphs are stored as a string of glyphs,
	// forming a very wide image, but with height equal
	// to the glyph's height. E.g.:
	// +--------------------
	// |1|2|3|...|A|B|C|...
	// +--------------------
    u32* bitmap;

    // Glyphs for thsi font
    static const int MAX_GLYPHS = 96;
    BuiltInFontGlyph glyphs[MAX_GLYPHS];
};

class FontManager
{
friend class Renderer;
//friend class PS2Renderer;

public:

    enum BuiltInFontId
    {
        FONT_CONSOLAS_10,
        FONT_CONSOLAS_12,
        FONT_CONSOLAS_16,
        FONT_CONSOLAS_20,
        FONT_CONSOLAS_24,
        FONT_CONSOLAS_30,
        FONT_CONSOLAS_36,
        FONT_CONSOLAS_40,
        FONT_CONSOLAS_48,
        FONT_BJ_TEST,

        FONT_COUNT
    };

    FontManager() :
        texAtlas(NULL),
        globalTextScale(1.0f)
    {
    }

    void LoadAllBuiltInFonts()
    {
        for (int i = 0; i < FONT_COUNT; i++)
        {
            if (!LoadBuiltInFont((BuiltInFontId)i))
            {
                printf("Error - failed to load builtin font\n");
            }
        }
    }

    void UnloadAllBuiltInFonts()
    {
        for (int i = 0; i < FONT_COUNT; i++)
        {
            UnloadBuiltInFont((BuiltInFontId)i);
        }
    }

    bool LoadBuiltInFont(const BuiltInFontId fontId)
    {
        if (IsBuiltInFontLoaded(fontId))
        { 
            printf("Built in bitmap font already loaded\n");
            return true;
        }

        if (texAtlas == NULL)
        {
            printf("ERROR - tex atlas is null\n");
            return false;
        }
        if (!texAtlas->IsInitialized())
        {
            printf("Initializing tex atlas\n");
            // width, height, bytes per pixel, default fill value
            texAtlas->Init( Texture::MAX_SIZE, Texture::MAX_SIZE, 4, 0 );
        }
        else
        {
            printf("NOT initializing tex atlas\n");
        }
        

        LoadGlyphs(fontId);

        if (!AllocateAtlasSpace(fontId))
        {
            printf("Failed to allocate atlas space\n");
            return false;
        }

        printf("Success loading font %d\n", (int)fontId);
        return true;
    }

    void UnloadBuiltInFont(const BuiltInFontId fontId)
    {
        UnloadGlyphs(fontId);
    }

    void DecompressGlyphs(const BuiltInFontId fontId)
    {
        // Based on code found at: http://silverspaceship.com/inner/imgui/grlib/gr_extra.c
	    // Originally written by Sean Barrett.

        #define SKIPBIT() \
        if ((buffer >>= 1), (--bitsLeft == 0)) { buffer = *source++; bitsLeft = 32; }

        #define PACKRGBA(r,g,b,a) \
        (u32)((((a) & 0xFF) << 24) | (((b) & 0xFF) << 16) | (((g) & 0xFF) << 8) | ((r) & 0xFF))

        BuiltInFontBitmap* font = &builtInFonts[fontId];
        font->numGlyphs = BuiltInFontBitmap::MAX_GLYPHS;

        const u32* source = compressedFontData[fontId];

        s32 i, j;
        s32 effectiveCount;
        s32 bitsLeft;
        s32 buffer;

        const s32 start = (source[0] >> 0) & 0xFF;
        const s32 count = (source[0] >> 8) & 0xFF;
        const s32 h     = (source[0] >> 16) & 0xFF;

        BuiltInFontGlyph* glyphs = &font->glyphs[0];
        s32 numGlyphs = font->numGlyphs;

        if (count > numGlyphs)
        {
            effectiveCount = numGlyphs;
        }
        else
        {
            effectiveCount = count;
        }
        
        font->charStart = start;
        font->bmpHeight = h;
        font->bmpWidth = 0;
        ++source;

        // count bitmap size
        for (i = 0; i < effectiveCount; ++i)
        {
            const s32 w = (source[i >> 2] >> ((i & 3) << 3)) & 0xFF;
            font->bmpWidth += w;
        }

        // allocate memory
        font->bitmap = (u32*)memalign(128, font->bmpWidth * font->bmpHeight);

        // set glyphs
        u32* pBitmap = font->bitmap;
        for (i = 0; i < effectiveCount; i++)
        {
            const s32 w = (source[i >> 2] >> ((i & 3) << 3)) & 0xFF;
            glyphs[i].w = w;
            glyphs[i].h = h;
            glyphs[i].pixels = pBitmap;
            pBitmap += w * h;
        }

        source += (count + 3) >> 2;
        buffer = *source++;
        bitsLeft = 32;

        while (--numGlyphs >= 0)
        {
            u32* c = glyphs->pixels;
            for (j = 0; j < glyphs->h; ++j)
            {
                s32 z = buffer & 1;
                SKIPBIT();
                if (!z)
                {
                    for (i = 0; i < glyphs->w; ++i)
                    {
                        *c++ = PACKRGBA(255, 255, 255, 0);
                    }
                }
                else
                {
                    for (i = 0; i < glyphs->w; ++i)
                    {
                        z = buffer & 1;
                        SKIPBIT();
                        if (!z)
                        {
                            *c++ = PACKRGBA(255, 255, 255, 0);
                        }
                        else
                        {
                            s32 n = 0;
                            n += n + (buffer & 1); SKIPBIT();
                            n += n + (buffer & 1); SKIPBIT();
                            n += n + (buffer & 1); SKIPBIT();
                            n += 1;
                            *c++ = PACKRGBA(255, 255, 255, ((255 * n) >> 3));
                        }
                        
                    }
                }
            }
            ++glyphs;
        }

        font->numGlyphs = count;

        #undef PACKRGBA
        #undef SKIPBIT
    }

    void LoadGlyphs(const BuiltInFontId fontId)
    {
        // assert fontId >= 0 && fontId < FONT_COUNT
        // assert builtInFonts[fontId].bitmap == NULL && "Font already loaded"

        // expand the data and allocate the bitmap
        DecompressGlyphs(fontId);
    }

    void UnloadGlyphs(const BuiltInFontId fontId)
    {
        // assert fontId >= 0 && fontId < FONT_COUNT
        if (builtInFonts[fontId].bitmap == NULL)
        {
            return;
        }

        free(builtInFonts[fontId].bitmap);
        builtInFonts[fontId].numGlyphs = 0;
        builtInFonts[fontId].charStart = 0;
        builtInFonts[fontId].bmpWidth = 0;
        builtInFonts[fontId].bmpHeight = 0;
        builtInFonts[fontId].bitmap = NULL;

        memset(builtInFonts[fontId].glyphs, 0, BuiltInFontBitmap::MAX_GLYPHS * sizeof(BuiltInFontGlyph));
    }

    bool AllocateAtlasSpace(const BuiltInFontId fontId)
    {
        // assert(fontId >= 0 && fontId < FONT_COUNT)
        // assert texAtlas.isInitialized
        if (!texAtlas->IsInitialized())
        {
            printf("ERROR  - expected texture atlas to be initialized!\n");
            return false;
        }

        // reserve memory for every glyph
        texAtlas->ReserveMemForRegions(builtInFonts[fontId].numGlyphs + 1);

        for (int i = 0; i < builtInFonts[fontId].numGlyphs; i++)
        {
            BuiltInFontGlyph& glyph = builtInFonts[fontId].glyphs[i];
            if (glyph.pixels == NULL)
            {
                continue;
            }

            // atlas space +1 pixel on each side for safety border when sampling
            const int BorderSize = 1;
            Rect4i region = texAtlas->AllocRegion(glyph.w + (BorderSize * 2), glyph.h + (BorderSize * 2));
            if (region.x < 0)
            {
                printf("Default tex atlas is full! discarding further text glyphs\n");
                return false;
            }

            // fill the region with default initial value
            texAtlas->SetRegion(region.x, region.y, region.width, region.height, 0);

            // copy pixels; preserve border
            region.x += BorderSize;
            region.y += BorderSize;
            texAtlas->SetRegion(region.x, region.y, glyph.w, glyph.h, (u8*)glyph.pixels, glyph.w * sizeof(u32));

            glyph.u0 = region.x;
            glyph.v0 = region.y;
            glyph.u1 = region.x + glyph.w;
            glyph.v1 = region.y + glyph.h;
        }

        return true;
    }

    float GetWhiteSpaceWidth(const BuiltInFontId fontId) const
    {
        //ps2assert(fontId >= 0 && fontId < FONT_COUNT)
        return (float)(builtInFonts[fontId].glyphs[0].w) * globalTextScale;
    }

    static const u32* const compressedFontData[10];
    static BuiltInFontBitmap builtInFonts[FONT_COUNT];

private:
    TextureAtlas* texAtlas;
    float globalTextScale;

    bool IsBuiltInFontLoaded(const BuiltInFontId fontId) const
    {
        // assert(fontId >= 0 && fontId < FONT_COUNT)
        return (builtInFonts[fontId].bitmap != NULL);
    }
};

BuiltInFontBitmap FontManager::builtInFonts[FontManager::FONT_COUNT];

const u32* const FontManager::compressedFontData[10] __attribute__((aligned(16))) = 
{
    font_10Con,
    font_12Con,
    font_16Con,
    font_20Con,
    font_24Con,
    font_30Con,
    font_36Con,
    font_40Con,
    font_48Con,
    font_21
};

#endif // FONTMGR_H_INCLUDED
