#ifndef THIRD_PERSON_CAMERA_H_INCLUDED
#define THIRD_PERSON_CAMERA_H_INCLUDED

#include <CameraBase.h>
#include <GameTime.h>

class ThirdPersonCamera : public CameraBase
{
public:

    enum MoveDir
    {
        FORWARD, BACK, RIGHT, LEFT, UP, DOWN
    };
    
    ThirdPersonCamera()
    {
        reset();
    }

    void Update(PS2Pad& gamePad, const PS2Vector& focusPoint)
    {
        if ( gamePad.getRightJoyX() > 200 ) {
            RotateRight();
        } else if ( gamePad.getRightJoyX() < 75 ) {
            RotateLeft();
        }

        if ( gamePad.getRightJoyY() > 200 ) {
            PitchUp();
        } else if ( gamePad.getRightJoyY() < 75 ) {
            PitchDown();
        }
        
        updateCalculations( focusPoint );
    }
    
    // Rotates around the X-axis by given radians
    void PitchUp()
    {
        pitchDegrees += (pitchRate * gTime.deltaTimeMillis);
        totalPitch += (pitchRate * gTime.deltaTimeMillis);
        if (totalPitch >= 360.0f)
        {
            totalPitch -= 360.0f;
        }
    }
    void PitchDown()
    {
        pitchDegrees -= (pitchRate * gTime.deltaTimeMillis);
        totalPitch -= (pitchRate * gTime.deltaTimeMillis);
        if (totalPitch < 0.0f)
        {
            totalPitch += 360.0f;
        }
    }
    
    // Rotates around the Y-axis by given radians
    void RotateRight()
    {
        yawDegrees -= (yawRate * gTime.deltaTimeMillis);
        totalYaw -= (yawRate * gTime.deltaTimeMillis);
        if (totalYaw < 0.0f)
        {
            totalYaw = 360.0f - totalYaw;
        }
    }
    void RotateLeft()
    {
        yawDegrees += (yawRate * gTime.deltaTimeMillis);
        totalYaw += (yawRate * gTime.deltaTimeMillis);
        if (totalYaw >= 360.0f)
        {
            totalYaw = 360.0f - totalYaw;
        }
    }
    
    PS2Matrix GetViewMatrix() const
    {
        PS2Matrix m;
        m.makeLookAt(eye, lastFocusPoint, up);
        return m;
    }
    PS2Vector GetCameraTarget() const
    {
        return PS2Vector(eye.x + forward.x,
                         eye.y + forward.y,
                         eye.z + forward.z,
                         1.0f);
    }

    void ZoomIn()
    {
        zoom(zoomRate * gTime.deltaTimeMillis);
    }
    void ZoomOut()
    {
        zoom(-(zoomRate * gTime.deltaTimeMillis));
    }
    
    void SetRightVec(const PS2Vector& right)     { this->right = right;     }
    void SetUpVec(const PS2Vector& up)           { this->up = up;           }
    void SetForwardVec(const PS2Vector& forward) { this->forward = forward; }
    void SetEyePosition(const PS2Vector& eyePos) { eye = eyePos;            }
    
    const PS2Vector& GetRightVec()    const { return right;   }
    const PS2Vector& GetUpVec()       const { return up;      }
    const PS2Vector& GetForwardVec()  const { return forward; }
    const PS2Vector& GetEyePosition() const { return eye;     }
    
    bool IsThirdPerson() { return true;  }
    bool IsFirstPerson() { return false; }
    
private:

    float yawDegrees;
    float totalYaw;
    float pitchDegrees;
    float totalPitch;
    float pitchRate;
    float yawRate;

    float cameraDistScale;
    float zoomRate;
    
    PS2Vector right;
    PS2Vector up;
    PS2Vector forward;
    PS2Vector eye;
    PS2Vector lastFocusPoint;

    inline float deg2rad(const float deg)
    {
        return deg * (M_PI/180.0f);
    }
    inline float clamp(const float val, const float min, const float max)
    {
        return (val < min) ? min :
               ((val > max) ? max : val);
    }

    void updateCalculations( const PS2Vector& focusPoint )
    {
        const float yawRadians = deg2rad( yawDegrees );
        const float pitchRadians = deg2rad( pitchDegrees );

        PS2Vector focusVector = eye - focusPoint;
        rotateAroundAxis( focusVector, focusVector, up, yawRadians );
        rotateAroundAxis( focusVector, focusVector, right, pitchRadians );

        const float MAX_CAMERA_DIST = 8.0f;
        const float MIN_CAMERA_DIST = 5.0f;
        focusVector *= cameraDistScale;
        focusVector.clampLength( MIN_CAMERA_DIST, MAX_CAMERA_DIST );

        eye = focusPoint + focusVector;
        lastFocusPoint = focusPoint;

        rotateAroundAxis( forward, forward, right, pitchRadians );
        rotateAroundCameraY( yawRadians );

        // apply damping to angles
        const float PITCH_YAW_FRAME_DAMPING = 0.01f;
        pitchDegrees *= PITCH_YAW_FRAME_DAMPING * gTime.deltaTimeMillis;
        yawDegrees *= PITCH_YAW_FRAME_DAMPING * gTime.deltaTimeMillis;
    }

    void reset()
    {
        // Manual adjustment - make it face north on init
        yawDegrees = 250.0f;
        pitchDegrees = 0.0f;

        totalYaw = 0.0f;
        yawRate = 0.05f;
        totalPitch = 0.0f;
        pitchRate = 0.02f;
        cameraDistScale = 1.0f;
        zoomRate = 0.001f;
        
        right   = PS2Vector(1.0f, 0.0f, 0.0f, 1.0f);
        up      = PS2Vector(0.0f, 1.0f, 0.0f, 1.0f);
        forward = PS2Vector(0.0f, 0.0f, 1.0f, 1.0f);
        eye     = PS2Vector(0.0f, 0.0f, 0.0f, 1.0f);
        lastFocusPoint = PS2Vector(0.0f, 0.0f, 0.0f, 1.0f);
    }

    void rotateAroundCameraY( const float angle )
    {
        const float sinAng = ps2math::sin( angle );
        const float cosAng = ps2math::cos( angle );

        // save vals
        float xxx = forward.x;
        float zzz = forward.z;
        
        // rotate forward vector
        forward.x = xxx * cosAng + zzz * sinAng;
        forward.z = xxx * -sinAng + zzz * cosAng;

        // save vals
        xxx = up.x;
        zzz = up.z;

        // rotate up vector
        up.x = xxx * cosAng + zzz * sinAng;
        up.z = zzz * -sinAng + zzz * cosAng;

        // save vals
        xxx = right.x;
        zzz = right.z;

        // rotate right vector
        right.x = xxx * cosAng + zzz * sinAng;
        right.z = xxx * -sinAng + zzz * cosAng;
    }

    void zoom(const float amount)
    {
        cameraDistScale = clamp(cameraDistScale + amount, 0.99f, 1.01f);
    }

};

#endif // THIRD_PERSON_CAMERA_H_INCLUDED
