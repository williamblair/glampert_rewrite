#ifndef GAME_TIME_H_INCLUDED
#define GAME_TIME_H_INCLUDED

// PS2DEV SDK:
#include <kernel.h>
#include <tamtypes.h>
#include <draw.h>
#include <graph.h>
#include <gs_psm.h>


#include <time.h>
#include <unistd.h>

static inline u32 millisecondsSinceStartup()
{
    return clock() / (CLOCKS_PER_SEC / 1000);
}

static inline float msecToSec(const float ms)
{
    return ms * 0.001f;
}

static inline float secToMsec(const float sec)
{
    return sec * 1000.0f;
}

static inline u32 secToMsec(const u32 sec)
{
    return sec * 1000;
}

// get the elapsed time in milliseconds since the last call to this function.
// NOT thread safe
static u32 clockMilliseconds()
{
    static u32 baseTime = 0;
    static u32 currentTime = 0;
    static bool timerInitialized = false;
    
    if (!timerInitialized)
    {
        baseTime = millisecondsSinceStartup();
        timerInitialized = true;
    }
    
    currentTime = millisecondsSinceStartup() - baseTime;
    return currentTime;
}

class GameTime
{
public:

    // elapsed since last frame
    float deltaTimeSeconds;
    float deltaTimeMillis;
    
    // real time since startup
    float currentTimeSeconds;
    u32   currentTimeMillis;
    
    GameTime()
    {
        Reset();
    }
    
    void BeginFrame()
    {
        t0ms = clockMilliseconds();
        deltaTimeSeconds = msecToSec(deltaTimeMillis);
        currentTimeSeconds = msecToSec(t0ms);
        currentTimeMillis = t0ms;
    }
    
    void EndFrame()
    {
        t1ms = clockMilliseconds();
        deltaTimeMillis = (float)(t1ms - t0ms);
    }
    
    void Reset()
    {
        deltaTimeSeconds = 0.0f;
        deltaTimeMillis = (1.0f / 60.0f); // assume init frame rate of 60fps
        currentTimeSeconds = 0.0f;
        currentTimeMillis = 0;
        t0ms = 0;
        t1ms = 0;
    }
    
private:
    GameTime(const GameTime&);
    GameTime& operator=(const GameTime&);
    
    u32 t0ms, t1ms;
};

// needs to be created in main.cpp
extern GameTime gTime;

#endif // GAME_TIME_H_INCLUDED
