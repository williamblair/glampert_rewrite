#ifndef MD2_MODEL_H_INCLUDED
#define MD2_MODEL_H_INCLUDED

#include <array.hpp>
#include <Aabb.h>

// ASM optimized packing of a DrawVertex:
static inline void packDrawVertex(DrawVertex & dv, 
                                  const PS2Vector & position, 
                                  const PS2Vector & texCoord, 
                                  const PS2Vector & color)
{
    asm volatile (
        "lqc2    vf4, 0x0(%1)  \n\t" // vf4 = position
        "lqc2    vf5, 0x0(%2)  \n\t" // vf5 = texCoord
        "lqc2    vf6, 0x0(%3)  \n\t" // vf6 = color
        "vmove.w vf4, vf0      \n\t" // vf4.w = 1.0 (set position.w to 1)
        "sqc2    vf6, 0x20(%0) \n\t" // dv.color    = vf6
        "sqc2    vf5, 0x10(%0) \n\t" // dv.texCoord = vf5
        "sqc2    vf4, 0x00(%0) \n\t" // dv.position = vf4
        : : "r" (&dv), "r" (&position), "r" (&texCoord), "r" (&color)
    );
}

class Md2AnimState
{
public:

    u32 startFrame;
    u32 endFrame; // last frame, inclusive (<=)
    u32 fps; // animation frames per second
    u32 curFrame;
    u32 nextFrame;
    float curTime;
    float oldTime;
    float interp; // percent of interpolation for this animation
    
    Md2AnimState()
    {
        Clear();
    }
    
    // should be called every frame with the current system time 
    // in seconds
    void Update(float timeSeconds, u32 keyframeCount)
    {
        curTime = timeSeconds;
        const u32 maxFrame = keyframeCount - 1;
        
        // calculate current and next frames
        if ((curTime - oldTime) > (1.0 / fps))
        {
            curFrame = nextFrame;
            nextFrame++;
            
            if (nextFrame > endFrame)
            {
                nextFrame = startFrame;
            }
            
            oldTime = curTime;
        }
        
        // prevent having current/next frame greater than total frames
        if (curFrame > maxFrame)
        {
            curFrame = 0;
        }
        if (nextFrame > maxFrame)
        {
            nextFrame = 0;
        }
        
        interp = fps * (curTime - oldTime);
    }
    
    void Clear()
    {
        startFrame = 0;
        endFrame = 0;
        fps = 0;
        curFrame = 0;
        nextFrame = 0;
        curTime = 0;
        oldTime = 0;
        interp = 0;
    }
};

// Used for non-standard animations.
const uint MD2ANIM_DEFAULT_FPS = 7;

// Standard MD2 animations, with predefined frame numbers and FPS.
enum StdMd2AnimId
{
    MD2ANIM_STAND,
    MD2ANIM_RUN,
    MD2ANIM_ATTACK,
    MD2ANIM_PAIN_A,
    MD2ANIM_PAIN_B,
    MD2ANIM_PAIN_C,
    MD2ANIM_JUMP,
    MD2ANIM_FLIP,
    MD2ANIM_SALUTE,
    MD2ANIM_FALL_BACK,
    MD2ANIM_WAVE,
    MD2ANIM_POINT,
    MD2ANIM_CROUCH_STAND,
    MD2ANIM_CROUCH_WALK,
    MD2ANIM_CROUCH_ATTACK,
    MD2ANIM_CROUCH_PAIN,
    MD2ANIM_CROUCH_DEATH,
    MD2ANIM_DEATH_FALL_BACK,
    MD2ANIM_DEATH_FALL_FORWARD,
    MD2ANIM_DEATH_FALL_BACK_SLOW,
    MD2ANIM_BOOM,

    // Number of entries in this enum. Internal use.
    MD2ANIM_COUNT
};

class Md2Model
{
public:

    Md2Model() :
        md2Header(NULL),
        md2Skins(NULL),
        md2TexCoords(NULL),
        md2Triangles(NULL),
        md2Keyframes(NULL),
        md2TriangleCount(0),
        md2VertesPerFrame(0),
        md2FrameCount(0),
        md2Scale(1.0f)
    {
    }

    // Load from a MD2 file data stored in memory. The memory is reference so it
    // needs to exist for the lifetime of this object
    bool InitFromMemory(const u8* data, u32 sizeBytes, float scale = 1.0f)
    {
        if (data == NULL || sizeBytes == 0)
        {
            printf("Invalid data buffer\n");
            return false;
        }
        
        printf("Importing MD2 from data buffer\n");
        
        // read Header
        md2Header = (Header*)data;
        if (md2Header->magic != chars2u32('I','D','P','2'))
        {
            printf("MD2 Header magic invalid, continuing\n");
        }
        if (md2Header->version != MD2_GOOD_VERSION)
        {
            printf("Bad MD2 version %u, want %u\n", md2Header->version, MD2_GOOD_VERSION);
        }
        
        if (md2Header->frameCount == 0)
        {
            printf("MD2 Frame count 0!\n");
            return false;
        }
        if (md2Header->offsetEnd > sizeBytes)
        {
            printf("Md2 buffer too small\n");
            return false;
        }
        
        if (md2Header->texCoordCount == 0)
        {
            printf("Md2 bad tex coord count\n");
            return false;
        }
        if (md2Header->triangleCount == 0)
        {
            printf("Md2 bad triangle count\n");
            return false;
        }
    
        if ((md2Header->offsetSkins     + md2Header->skinCount)     * sizeof(Skin) >= sizeBytes     || 
            (md2Header->offsetTexCoords + md2Header->texCoordCount) * sizeof(TexCoord) >= sizeBytes ||
            (md2Header->offsetTris      + md2Header->triangleCount) * sizeof(Triangle) >= sizeBytes)
        {
            printf("MD2 header bad offsets\n");
            return false;
        }
    
        md2Scale = scale;
        
        md2TriangleCount = md2Header->triangleCount;
        md2VertesPerFrame = md2Header->vertsPerFrame;
        md2FrameCount = md2Header->frameCount;
        
        if (md2Header->skinCount >= MD2_MAX_SKINS)
        {
            printf("Model data contains more skin than Quake II supports\n");
            return false;
        }
        if (md2Header->frameCount >= MD2_MAX_KEYFRAMES)
        {
            printf("Model data contains more keyframes than Quake II supports\n");
            return false;
        }
        if (md2Header->vertsPerFrame >= MD2_MAX_VERTS)
        {
            printf("Model data contains more vertices than Quake II supports\n");
            return false;
        }
        if (md2Header->triangleCount >= MD2_MAX_TRIS)
        {
            printf("Model data contains more triangles than Quake II supports\n");
            return false;
        }
    
        printf("MD2 importing skins, texCoords, tris\n");
    
        if (md2Header->skinCount != 0)
        {
            md2Skins = (Skin*)(data + md2Header->offsetSkins);
        }
        
        // read texture coordinates and triangles (mandatory)
        md2TexCoords = (const TexCoord*)(data + md2Header->offsetTexCoords);
        md2Triangles = (const Triangle*)(data + md2Header->offsetTris);
        
        printf("Importing MD2 keyframes\n");
    
        // note - each keyframe is followed by a vertex packet
        md2Keyframes = (const Keyframe*)(data + md2Header->offsetFrames);
        
        // TODO
        // pre-allocate a bounding box for each frame
        // md2FrameBounds.resize(md2FrameCount);
        for (u32 f = 0; f < md2FrameCount; f++)
        {
            if ((f * md2VertesPerFrame) >= (md2FrameCount * md2VertesPerFrame))
            {
                printf("Bad vertex offset in Md2 frame %u\n", f);
                return false;
            }
            // computeAabbForFrame(f);
        }
        
        // TODO
        //setupAnimations();
        printf("MD2 import completed\n");
        return true;
    }
    
    // Generates a vertex array for the given animation frame. drawVerts[] must be triangleCount*3 size
    void AssembleFrame(u32 frameIndex, 
                        u32 skinWidth, u32 skinHeight, 
                        const Color4f& vertColor, 
                        DrawVertex* drawVerts) const
    {
        //assert(md2Header != NULL)
        //assert(md2TexCoords != NULL)
        //assert(md2Triangles != NULL)
        //assert(md2Keyframes != NULL)
        //assert(drawVerts != NULL)
        
        const Keyframe& frame = GetKeyframe(frameIndex);
        const PS2Vector vColor(vertColor.r, vertColor.g, vertColor.b, vertColor.a);
        
        //printf("MD2 scale: %f\n", md2Scale);
        
        // for each triangle
        for (u32 t = 0; t < md2TriangleCount; t++)
        {
            // for each triangle vertex
            for (u32 v = 0; v < 3; v++)
            {
                // fetch correct vertex and tex coords
                const Vertex& vert = GetFrameVertex(frameIndex, md2Triangles[t].vertex[v]);
                const TexCoord& texc = md2TexCoords[md2Triangles[t].uv[v]];
                
                // compute final tex coords
                const float uvX = ((float)texc.u) / skinWidth;
                const float uvY = ((float)texc.v) / skinHeight;
                
                // uncompress the vertex position and transform
                // swap y-z because Quake's coord system is different from standard setup
                PS2Vector xyzPos;
                xyzPos.x = ((frame.scale[0] * vert.v[0]) + frame.translate[0]) * md2Scale;
                xyzPos.z = ((frame.scale[1] * vert.v[1]) + frame.translate[1]) * md2Scale;
                xyzPos.y = ((frame.scale[2] * vert.v[2]) + frame.translate[2]) * md2Scale;
                
                // store the new drawVertex
                packDrawVertex(drawVerts[v], xyzPos, PS2Vector(uvX, uvY, 0.0f, 1.0f), vColor);
            }
            
            drawVerts += 3;
        }
    }
    
    // Generate vertex array for given frame, from the compressed MD2 frame data.
    // drawVerts[] must be triangleCount*3 size
    void AssembleFrameInterpolated(u32 frameA, u32 frameB, 
                                    float interp, 
                                    u32 skinWidth, u32 skinHeight, 
                                    const Color4f& vertColor, 
                                    DrawVertex* drawVerts) const
    {
        // assert(md2Header != NULL)
        // assert(md2TexCoords != NULL)
        // assert(md2Triangles != NULL)
        // assert(md2Keyframes != NULL)
        // assert(drawVertex != NULL)
        
        const Keyframe& keyframeA = GetKeyframe(frameA);
        const Keyframe& keyframeB = GetKeyframe(frameB);
        const PS2Vector vColor(vertColor.r, vertColor.g, vertColor.b, vertColor.a);
        
        // for each triangle
        for (u32 t = 0; t < md2TriangleCount; t++)
        {
            // for each triangle vertex
            for (u32 v = 0; v < 3; v++)
            {
                const Vertex& vertA = GetFrameVertex(frameA, md2Triangles[t].vertex[v]);
                const Vertex& vertB = GetFrameVertex(frameB, md2Triangles[t].vertex[v]);
                const TexCoord& texc = md2TexCoords[md2Triangles[t].uv[v]];
                
                // compute final tex coords
                const float uvX = ((float)texc.u) / skinWidth;
                const float uvY = ((float)texc.v) / skinHeight;
                
                // compute final vertex position
                // swap y and z because Quake's coordinate system is different
                PS2Vector vecA;
                PS2Vector vecB;
                vecA.x = (keyframeA.scale[0] * vertA.v[0]) + keyframeA.translate[0];
                vecA.z = (keyframeA.scale[1] * vertA.v[1]) + keyframeA.translate[1];
                vecA.y = (keyframeA.scale[2] * vertA.v[2]) + keyframeA.translate[2];
                vecB.x = (keyframeB.scale[0] * vertB.v[0]) + keyframeB.translate[0];
                vecB.z = (keyframeB.scale[1] * vertB.v[1]) + keyframeB.translate[1];
                vecB.y = (keyframeB.scale[2] * vertB.v[2]) + keyframeB.translate[2];
                
                // linear interpolate final position and scale
                PS2Vector xyzPos;
                lerpScale(xyzPos, vecA, vecB, interp, md2Scale);
                
                // store the new drawVertex
                packDrawVertex(drawVerts[v], xyzPos, PS2Vector(uvX, uvY, 0.0f, 1.0f), vColor);
            }
            
            drawVerts += 3;
        }
    }
    
    // print list of animations to the console
    void PrintAnimList() const
    {
        printf("|  md2-anim-name  | start |  end  |\n");
        for (u32 i = 0; i < md2Anims.size(); i++)
        {
            printf("| %-15s | %-5u | %-5u |\n",
                    md2Anims[i].name,
                    md2Anims[i].start,
                    md2Anims[i].end);
        }
        printf("Listed %u animations\n", md2Anims.size());
    }

    // find animation by exact name. fps might be an estimate
    bool FindAnimByName(const char* name, u32& start, u32& end, u32& fps) const
    {
        //assert(name != NULL)
        for (u32 i = 0; i < md2Anims.size(); i++)
        {
            if (strcmp(md2Anims[i].name, name) == 0)
            {
                start = md2Anims[i].start;
                end = md2Anims[i].end;
                fps = GetAnimFpsForRange(start, end);
                return true;
            }
        }
        
        // failed to find
        start = 0;
        end = 0;
        fps = 0;
        return false;
    }
    // find animation by partial name. fps might be an estimate
    bool FindAnimByPartialName(const char* partialName, u32& start, u32& end, u32& fps) const
    {
        //assert(partialName != NULL);
        
        for (u32 i = 0; i < md2Anims.size(); i++)
        {
            if (strstr(md2Anims[i].name, partialName) != NULL)
            {
                start = md2Anims[i].start;
                end = md2Anims[i].end;
                fps = GetAnimFpsForRange(start, end);
                return true;
            }
        }
        
        // failed to find
        start = 0;
        end = 0;
        fps = 0;
        return false;
    }

    // get animation by its index in the anim list. index range is 0 to getAnimCount() - 1
    bool FindAnimByIndex(u32 index, u32& start, u32& end, u32& fps) const
    {
        if (index >= md2Anims.size())
        {
            start = 0;
            end = 0;
            fps = 0;
            return false;
        }
        
        start = md2Anims[index].start;
        end = md2Anims[index].end;
        fps = GetAnimFpsForRange(start, end);
        return true;
    }
    
    // Get the range and frame rate of a standard MD2 animation
    static void GetStdAnim(StdMd2AnimId id, u32& start, u32& end, u32& fps)
    {
        // assert(id >= 0 && id < MD2ANIM_COUNT);
        start = stdMd2Anims[id].start;
        end = stdMd2Anims[id].end;
        fps = stdMd2Anims[id].fps;
    }

    // Get the frame rate of an animation if it is in within a known range; default val otherwise
    static u32 GetAnimFpsForRange(u32 start, u32 end)
    {
        for (u32 i = 0; i < MD2ANIM_COUNT; i++)
        {
            if (start == stdMd2Anims[i].start &&
                end == stdMd2Anims[i].end)
            {
                return stdMd2Anims[i].fps;
            }
        }
        return MD2ANIM_DEFAULT_FPS;
    }

    // Bounding box for a given frame
    const Aabb& GetBoundsForFrame(u32 frameIndex) const
    {
        return md2FrameBounds[frameIndex];
    }

    u32 GetKeyframeCount() const { return md2FrameCount;                     }
    u32 GetVertexCount() const   { return md2FrameCount * md2VertesPerFrame; }
    u32 GetTriangleCount() const { return md2TriangleCount;                  }
    u32 GetAnimCount() const     { return md2Anims.size();                   }
    float GetModelScale() const  { return md2Scale;                          }

private:

    Md2Model(const Md2Model& );
    Md2Model & operator=(const Md2Model& );
    
    // MD2 constants
    static const u32 MD2_GOOD_VERSION   = 8; // Version used in Quake II
    static const u32 MD2_MAX_VERTS      = 2048; // per keyframe
    static const u32 MD2_MAX_TEXCOORDS  = 2048; // for the whole model
    static const u32 MD2_MAX_TRIS       = 4096; // for the whole model
    static const u32 MD2_MAX_KEYFRAMES  = 512; // for the whole model
    static const u32 MD2_MAX_SKINS      = 32; // for the whole model
    static const u32 MD2_MAX_NAME_CHARS = 64; // skins/anims
    
    // MD2 Header
    struct __attribute__((packed)) Header
    {
        u32 magic;
        u32 version;
        u32 skinWidth;
        u32 skinHeight;
        u32 frameSize;        // size of a single frame in bytes
        u32 skinCount;
        u32 vertsPerFrame;
        u32 texCoordCount;
        u32 triangleCount;
        u32 glCmdCount;
        u32 frameCount;       
        u32 offsetSkins;      // offset to skin data
        u32 offsetTexCoords;  // offset to texture coords
        u32 offsetTris;       // offset to triangle data
        u32 offsetFrames;     // offset to frame data
        u32 offsetGLCmds;     // offset to OpenGL commands
        u32 offsetEnd;        // offset to the end of the file
    };
    
    // skin texture filename
    struct __attribute__((packed)) Skin
    {
        char name[MD2_MAX_NAME_CHARS]; // null terminated filename str
    };
    
    // texture coordinates (uv)
    struct __attribute__((packed)) TexCoord
    {
        u16 u, v;
    };
    
    // Triangle data
    struct __attribute__((packed)) Triangle
    {
        u16 vertex[3]; // trianglex vertex indices
        u16 uv[3]; // texture coord indices
    };
    
    // Vertex data
    struct __attribute__((packed)) Vertex
    {
        u8 v[3]; // compressed vertex position
        u8 normalIndex; // normal vector index into emblematic MD2 normals table
    };
    
    // Keyframe data
    struct __attribute__((packed)) Keyframe
    {
        float scale[3]; // scale factors
        float translate[3]; // translation vector
        char name[16]; // frame name
        
        // followed by a vertex packet:
        // Vertex frameVerts[header.vertsPerFrame];
    };
    
    // animation info (frame indices and anim name):
    struct __attribute__((packed)) Anim
    {
        u32 start, end;
        char name[MD2_MAX_NAME_CHARS];
    };
    
    // Internal helpers
    
    void SetupAnimations()
    {
        Anim animInfo;
        char frameAnim[MD2_MAX_NAME_CHARS * 2];
        char currentAnim[MD2_MAX_NAME_CHARS * 2];
        
        memset(&animInfo, 0, sizeof(animInfo));
        memset(currentAnim, 0, sizeof(currentAnim));
        
        md2Anims.reserve(md2FrameCount);
        for (u32 i = 0; i < md2FrameCount; i++)
        {
            memset(frameAnim, 0, sizeof(frameAnim));
            const char* frameName = GetKeyframe(i).name;
            
            // extract the animation name from frame name:
            // MD2 anim names are in the form <animName><frameNum>, like "death001"
            //
            // strcspn() will find the index of the first occurance of any number in the frame name
            size_t len = strcspn(frameName, "0123456789");
            if ((strlen(frameName) - 3 == len) && (frameName[len] != '\0'))
            {
                ++len;
            }
            
            strncpy(frameAnim, frameName, len);
            
            if (strcmp(currentAnim, frameAnim) != 0)
            {
                if (i > 0)
                {
                    // prev anim finished, store it and start new
                    strncpy(animInfo.name, currentAnim, sizeof(animInfo.name));
                    md2Anims.pushBack(animInfo);
                }
                
                // initialize new anim info
                animInfo.start = i;
                animInfo.end = i;
                strncpy(currentAnim, frameAnim, sizeof(currentAnim));
            }
            else
            {
                animInfo.end = i;
            }
        }
        
        // add the last animation
        strncpy(animInfo.name, currentAnim, sizeof(animInfo.name));
        md2Anims.pushBack(animInfo);
    }
    
    void ComputeAabbForFrame(u32 frameIndex)
    {
        Aabb& frameAabb = md2FrameBounds[frameIndex];
        frameAabb.Clear();
        
        const Keyframe& keyframe = GetKeyframe(frameIndex);
        
        for (u32 t = 0; t < md2TriangleCount; t++)
        {
            for (u32 v = 0; v < 3; v++)
            {
                const Vertex& vert = GetFrameVertex(frameIndex, md2Triangles[t].vertex[v]);
                
                // uncompress vertex position and transform it, with swapping y/z
                PS2Vector xyzPos;
                xyzPos.x = (keyframe.scale[0] * vert.v[0] + keyframe.translate[0]) * md2Scale;
                xyzPos.z = (keyframe.scale[1] * vert.v[1] + keyframe.translate[1]) * md2Scale;
                xyzPos.y = (keyframe.scale[2] * vert.v[2] + keyframe.translate[2]) * md2Scale;
                xyzPos.w = 1.0f;
                
                // Add to bounds if new min/max
                frameAabb.mins = min3PerElement(xyzPos, frameAabb.mins);
                frameAabb.maxs = max3PerElement(xyzPos, frameAabb.maxs);
            }
        }
    }
    const Keyframe& GetKeyframe(u32 frameIndex) const
    {
        //assert(frameIndex < md2FrameCount)
        
        const u8* framesPtr = (const u8*)(md2Keyframes);
        
        // each keyframe is a single `Keyframe` struct + a vertex packet with md2VertesPerFrame vertices
        const u32 keyframePacketSize = sizeof(Keyframe) + (sizeof(Vertex) * md2VertesPerFrame);
        
        // skip to the desired frame
        return *((const Keyframe*)(framesPtr + (frameIndex * keyframePacketSize)));
    }
    const Vertex& GetFrameVertex(u32 frameIndex, u32 vertexIndex) const
    {
        // assert(frameIndex < md2FrameCount);
        
        const u8* vertsPtr = (u8*)md2Keyframes;
        
        // each keyframe is a single 'Keyframe' struct + a vertex packet with md2VertsPerFrame vertices
        const u32 keyframePacketSize = sizeof(Keyframe) + (sizeof(Vertex) * md2VertesPerFrame);
        
        // skip to the desired frame
        vertsPtr += frameIndex * keyframePacketSize;
        
        // move forward to the vertices
        vertsPtr += sizeof(Keyframe);
        
        return ((const Vertex*)(vertsPtr))[vertexIndex];
    }
    
private:

    // extension data pointers
    const Header*   md2Header;
    const Skin*     md2Skins;
    const TexCoord* md2TexCoords;
    const Triangle* md2Triangles;
    const Keyframe* md2Keyframes;
    
    // cache for quick access
    u32 md2TriangleCount;
    u32 md2VertesPerFrame;
    u32 md2FrameCount;
    
    // scaling applied to model vertices; 1.0 by default
    float md2Scale;
    
    // Data owned by Md2Model
    Array<Anim> md2Anims;
    Array<Aabb> md2FrameBounds;
    
    static u32 chars2u32(char a, char b, char c, char d)
    {
        return (((u32)a) << 0) |
               (((u32)b) << 8) |
               (((u32)c) << 16) |
               (((u32)d) << 24);
    }
    
    struct Md2Anim
    {
        u8 start;
        u8 end;
        u8 fps;
    };
    static Md2Anim stdMd2Anims[MD2ANIM_COUNT] __attribute__((aligned(16)));
};

Md2Model::Md2Anim Md2Model::stdMd2Anims[MD2ANIM_COUNT] __attribute__((aligned(16))) =
{
    {   0,    39,   9 }, // Stand
    {  40,    45,  10 }, // Run
    {  46,    53,  10 }, // Attack
    {  54,    57,   7 }, // Pain A
    {  58,    61,   7 }, // Pain B
    {  62,    65,   7 }, // Pain C
    {  66,    71,   7 }, // Jump
    {  72,    83,   7 }, // Flip
    {  84,    94,   7 }, // Salute
    {  95,   111,  10 }, // Fall back
    { 112,   122,   7 }, // Wave
    { 123,   134,   6 }, // Point
    { 135,   153,  10 }, // Crouch stand
    { 154,   159,   7 }, // Crouch walk
    { 160,   168,  10 }, // Crouch attack
    { 196,   172,   7 }, // Crouch pain
    { 173,   177,   5 }, // Crouch death
    { 178,   183,   7 }, // Death fall back
    { 184,   189,   7 }, // Death fall forward
    { 190,   197,   7 }, // Death fall back slow
    { 198,   198,   5 }  // Boom
};

#endif // MD2_MODEL_H_INCLUDED

