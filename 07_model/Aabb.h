#ifndef AABB_H_INCLUDED
#define AABB_H_INCLUDED

#include <PS2Vector.h>

// simple axis aligned bounding box
// Has a min point and a max point for the two extents of a box
struct __attribute__((aligned(16))) Aabb
{
    inline Aabb() :
        mins(0.0f, 0.0f, 0.0f, 1.0f),
        maxs(0.0f, 0.0f, 0.0f, 1.0f)
    {
    }
    inline Aabb(const float min[], const float max[]) :
        mins(min[0], min[1], min[2], 1.0f),
        maxs(max[0], max[1], max[2], 1.0f)
    {
    }
    inline Aabb(const PS2Vector& min, const PS2Vector& max) :
        mins(min),
        maxs(max)
    {
    }
    inline Aabb(const void* vertices, u32 vertexCount, u32 vertexStride)
    {
        FromMeshVertices(vertices, vertexCount, vertexStride);
    }
    
    // Inside out bounds
    inline void Clear()
    {
        const float INF = 1e30f; // large float, HUGE_VAL unavailable
        mins = PS2Vector(INF, INF, INF, 1.0f);
        maxs = PS2Vector(-INF, -INF, -INF, 1.0f);
    }
    
    // Set as a point at the origin
    inline void SetZero()
    {
        mins = PS2Vector(0.0f, 0.0f, 0.0f, 1.0f);
        maxs = PS2Vector(0.0f, 0.0f, 0.0f, 1.0f);
    }
    
    // Returns true if bounds are inside out
    inline bool IsCleared() const
    {
        return (mins.x > maxs.x);
    }
    
    // returns the volume of the bounds
    inline float GetVolume() const
    {
        if ((mins.x >= maxs.x) || (mins.y >= maxs.y) || (mins.z >= maxs.z))
        {
            return 0.0f;
        }
        return ((maxs.x - mins.x) * (maxs.y - mins.y) * (maxs.z - mins.z));
    }
    
    inline PS2Vector GetCenter() const
    {
        return ((maxs + mins) / 0.5f);
    }
    
    // Test if a point is touching or inside the box bounds
    inline bool ContainsPoint(const PS2Vector& boxPos, const PS2Vector& p)
    {
        if (p.x < mins.x + boxPos.x ||
            p.x > maxs.x + boxPos.x ||
            p.y < mins.y + boxPos.y ||
            p.y > maxs.y + boxPos.y ||
            p.z < mins.z + boxPos.z ||
            p.z > maxs.z + boxPos.z)
        {
            return false;
        }
        return true;
    }
    
    // returns true if the line intersects the bounds between the start and end points
    inline bool LineIntersection(const PS2Vector& start, const PS2Vector& end) const
    {
        const PS2Vector center     = (mins + maxs) * 0.5f;
        const PS2Vector extents    = maxs - center;
        const PS2Vector lineDir    = (end - start) * 0.5f;
        const PS2Vector lineCenter = start + lineDir;
        const PS2Vector dir        = lineCenter - center;
        
        const float ld0 = ps2math::abs(lineDir.x);
        if (ps2math::abs(dir.x) > (extents.x + ld0))
        {
            return false;
        }
        
        const float ld1 = ps2math::abs(lineDir.y);
        if (ps2math::abs(dir.y) > (extents.y + ld1))
        {
            return false;
        }
        
        const float ld2 = ps2math::abs(lineDir.z);
        if (ps2math::abs(dir.z) > (extents.z + ld2))
        {
            return false;
        }
        
        const PS2Vector vCross = crossProduct(lineDir, dir);
        if (ps2math::abs(vCross.x) > (extents.y * ld2 + extents.z * ld1))
        {
            return false;
        }
        
        if (ps2math::abs(vCross.y) > (extents.x * ld2 + extents.z * ld0))
        {
            return false;
        }
        
        if (ps2math::abs(vCross.z) > (extents.x * ld1 + extents.y * ld0))
        {
            return false;
        }
        
        
        return true;
    }
    
    // Build the tightest bounds for a set of mesh vertices. 
    // First element of the vertex must be a Vec3 (the vertex position)
    inline Aabb& FromMeshVertices(const void* vertices, u32 vertexCount, u32 vertexStride)
    {
        // assert(vertices != NULL)
        // assert(vertexCount != 0 && vertexStride != 0);
        
        Clear();
        const u8* ptr = (u8*)vertices;
        for (u32 i = 0; i < vertexCount; i++)
        {
            const PS2Vector* v = (const PS2Vector*)(ptr + i * vertexStride);
            const PS2Vector xyz(v->x, v->y, v->z, 1.0f);
            
            // selects the min/max x,y,z from each vector
            mins = min3PerElement(xyz, mins);
            maxs = max3PerElement(xyz, maxs);
        }
        
        return *this;
    }
    
    // Scale the internal vertices
    inline Aabb& Scale(float s)
    {
        mins *= s;
        maxs *= s;
        return *this;
    }
    
    // Transform the points defining this bounds by a given matrix
    inline Aabb& Transform(const PS2Matrix& mat)
    {
        PS2Vector tmp;
        
        tmp = mat * mins;
        mins = tmp;
        
        tmp = mat * maxs;
        maxs = tmp;
        
        return *this;
    }
    
    // Get a transformed copy of this Aabb
    inline Aabb Transformed(const PS2Matrix& mat) const
    {
        Aabb dummy(*this);
        dummy.Transform(mat);
        return dummy;
    }
    
    // Turn the Aabb into a set of points defining a box
    inline void ToPoints(PS2Vector points[8]) const
    {
        const PS2Vector b[2] = { maxs, mins };
        for (int i = 0; i < 8; i++)
        {
            points[i].x = b[(i ^ (i >> 1)) & 1].x;
            points[i].y = b[(i >> 1) & 1].y;
            points[i].z = b[(i >> 2) & 1].z;
            points[i].w = 1.0f;
        }
    }
    
    // Linearly interpolate two Aabbs
    static inline Aabb Lerp(const Aabb& a, const Aabb& b, const float t)
    {
        PS2Vector mins;
        mins.x = a.mins.x + t * (b.mins.x - a.mins.x);
        mins.y = a.mins.y + t * (b.mins.y - a.mins.y);
        mins.z = a.mins.z + t * (b.mins.z - a.mins.z);
        mins.w = 1.0f;
        
        PS2Vector maxs;
        maxs.x = a.maxs.x + t * (b.maxs.x - a.maxs.x);
        maxs.y = a.maxs.y + t * (b.maxs.y - a.maxs.y);
        maxs.z = a.maxs.z + t * (b.maxs.z - a.maxs.z);
        maxs.w = 1.0f;
        
        return Aabb(mins, maxs);
    }
    
    // Test if two Aabbs collide/intersect
    static bool Collision(const Aabb& box1, const PS2Vector& box1Pos, const Aabb& box2, const PS2Vector& box2Pos)
    {
        if ((box1.mins.x + box1Pos.x) > (box2.maxs.x + box2Pos.x) ||
            (box1.maxs.x + box1Pos.x) < (box2.mins.x + box2Pos.x))
        {
            return false;
        }
        if ((box1.mins.y + box1Pos.y) > (box2.maxs.y + box2Pos.y) ||
            (box1.maxs.y + box1Pos.y) < (box2.mins.y + box2Pos.y))
        {
            return false;
        }
        if ((box1.mins.z + box1Pos.z) > (box2.maxs.z + box2Pos.z) ||
            (box1.maxs.z + box1Pos.z) < (box2.mins.z + box2Pos.z))
        {
            return false;
        }
        
        return true;
    }
    
    // Minimum and maximum extents of the box
    PS2Vector mins;
    PS2Vector maxs;
};

#endif
