#ifndef FIRST_PERSON_CAMERA_H_INCLUDED
#define FIRST_PERSON_CAMERA_H_INCLUDED

#include <CameraBase.h>
#include <GameTime.h>

class FirstPersonCamera : public CameraBase
{
public:

    enum MoveDir
    {
        FORWARD, BACK, RIGHT, LEFT, UP, DOWN
    };
    
    FirstPersonCamera()
    {
        reset();
    }

    void Update(PS2Pad& gamePad, const PS2Vector& focusPoint)
    {
        if (gamePad.getLeftJoyX() >= 200)  Move(RIGHT);
        if (gamePad.getLeftJoyX() <= 50)   Move(LEFT);
        if (gamePad.getLeftJoyY() <= 50)   Move(FORWARD);
        if (gamePad.getLeftJoyY() >= 200)  Move(BACK);
        
        if (gamePad.getRightJoyX() >= 200) Rotate((-1.0f)*3.14159f/180.0f);
        if (gamePad.getRightJoyX() <= 50)  Rotate((1.0f)*3.14159f/180.0f);
        
        if (gamePad.getRightJoyY() <= 50) Pitch((-1.0f)*3.14159f/180.0f);
        if (gamePad.getRightJoyY() >= 200) Pitch((1.0f)*3.14159f/180.0f);
        
        // dampen angles - TODO
        const float PITCH_YAW_FRAME_DAMPING = 0.01f;
        pitchDegrees *= PITCH_YAW_FRAME_DAMPING * gTime.deltaTimeMillis;
        yawDegrees *= PITCH_YAW_FRAME_DAMPING * gTime.deltaTimeMillis;
    }
    
    // Rotates around the X-axis by given radians
    void Pitch(float radians)
    {
        rotateAroundAxis(forward, forward, right, radians);
        
        up = forward.cross(right);
    }
    
    // Rotates around the Y-axis by given radians
    void Rotate(float radians)
    {
        const float sinAng = ps2math::sin(radians);
        const float cosAng = ps2math::cos(radians);
        
        // save current foward x and z
        float xxx = forward.x;
        float zzz = forward.z;
        
        // rotate forward vector
        forward.x = xxx * cosAng  + zzz * sinAng;
        forward.z = xxx * -sinAng + zzz * cosAng;
        
        // save current up x and z;
        xxx = up.x;
        zzz = up.z;
        
        // rotate up vector
        up.x = xxx * cosAng  + zzz * sinAng;
        up.z = xxx * -sinAng + zzz * cosAng;
        
        // save current right x and z
        xxx = right.x;
        zzz = right.z;
        
        // rotate right vector
        right.x = xxx *  cosAng + zzz * sinAng;
        right.z = xxx * -sinAng + zzz * cosAng;
    }
    
    // Moves the camera position in the given direction
    void Move(const MoveDir dir)
    {
        const float amount = moveSpeed * gTime.deltaTimeMillis;
        
        switch (dir)
        {
        case FORWARD:
            eye.x += forward.x * amount;
            eye.y += forward.y * amount;
            eye.z += forward.z * amount;
            break;
        case BACK:
            eye.x -= forward.x * amount;
            eye.y -= forward.y * amount;
            eye.z -= forward.z * amount;
            break;
        case LEFT:
            eye.x += right.x * amount;
            eye.y += right.y * amount;
            eye.z += right.z * amount;
            break;
        case RIGHT:
            eye.x -= right.x * amount;
            eye.y -= right.y * amount;
            eye.z -= right.z * amount;
            break;
        case UP:
            eye.x += up.x * amount;
            eye.y += up.y * amount;
            eye.z += up.z * amount;
            break;
        case DOWN:
            eye.x -= up.x * amount;
            eye.y -= up.y * amount;
            eye.z -= up.z * amount;
            break;
        default:
            printf("Invalid move direction!\n");
            break;
        }
    }
    
    PS2Matrix GetViewMatrix() const
    {
        PS2Matrix m;
        m.makeLookAt(eye, GetCameraTarget(), up);
        return m;
    }
    PS2Vector GetCameraTarget() const
    {
        return PS2Vector(eye.x + forward.x,
                         eye.y + forward.y,
                         eye.z + forward.z,
                         1.0f);
    }
    
    void SetRightVec(const PS2Vector& right)     { this->right = right;     }
    void SetUpVec(const PS2Vector& up)           { this->up = up;           }
    void SetForwardVec(const PS2Vector& forward) { this->forward = forward; }
    void SetEyePosition(const PS2Vector& eyePos) { eye = eyePos;            }
    void SetMovementSpeed(const float speed)     { moveSpeed = speed;       }
    void SetTurnSpeed(const float speed)         { turnSpeed = speed;       }
    
    const PS2Vector& GetRightVec()    const { return right;   }
    const PS2Vector& GetUpVec()       const { return up;      }
    const PS2Vector& GetForwardVec()  const { return forward; }
    const PS2Vector& GetEyePosition() const { return eye;     }
    
    bool IsThirdPerson() { return false; }
    bool IsFirstPerson() { return true;  }
    
private:

    float moveSpeed;
    float turnSpeed;
    float pitchDegrees;
    float yawDegrees;
    float lastPitchDegrees;
    
    PS2Vector right;
    PS2Vector up;
    PS2Vector forward;
    PS2Vector eye;

    void reset()
    {
        moveSpeed = 0.005f;
        turnSpeed = 0.04f;
        pitchDegrees = 0.0f;
        yawDegrees = 0.0f;
        lastPitchDegrees = 0.0f;
        
        right   = PS2Vector(1.0f, 0.0f, 0.0f, 1.0f);
        up      = PS2Vector(0.0f, 1.0f, 0.0f, 1.0f);
        forward = PS2Vector(0.0f, 0.0f, 1.0f, 0.0f);
        eye     = PS2Vector(0.0f, 0.0f, 0.0f, 1.0f);
    }

};

#endif // FIRST_PERSON_CAMERA_H_INCLUDED
