#ifndef TEXTUREATLAS_H_INCLUDED
#define TEXTUREATLAS_H_INCLUDED

#include <array.hpp>

// PS2DEV SDK:
#include <draw.h>
#include <graph.h>
#include <gs_psm.h>

struct __attribute__((packed)) Vec3i
{
	int x, y, z;
};

struct __attribute__((packed)) Vec2f
{
    float x, y;
};

struct __attribute__((packed)) Rect4i
{
	int x, y;
	int width, height;
};

struct __attribute__((packed)) Color4b
{
	u8 r, g, b, a;
};

struct __attribute__((packed)) Color4f
{
    float r, g, b, a;
};

class TextureAtlas
{
public:

    TextureAtlas() :
        texture(),
        nodes(),
        pixels(NULL),
        usedPixels(0),
        width(0),
        height(0),
        bpp(0),
        initialized(false)
    {}
    
    ~TextureAtlas()
    {
        free(pixels);
    }

    bool Init(const int w, const int h, const int bpp, const u8 fillVal)
    {
        //assert(!initialized && "Already initialized");
        //assert(w != 0 && h != 0 && bpp != 0);

        printf("Calling tex atlas init\n");

        width = w;
        height = h;
        this->bpp = bpp;

        if (bpp != 4)
        {
            printf("Texture Atlas: Only supprts 4 bpp textures!\n");
            return false;
        }

        // make a one pixel border around the whole atlas to avoid artifacts when sampling
        const Vec3i tmp = { 1, 1, width - 2 };
        nodes.pushBack(tmp);

        // allocate cleared image
        pixels = (u8*)memalign(128, width * height * bpp);
        if (pixels == NULL)
        {
            printf("Tex atlas failed to memalign pixels\n");
        }
        memset(pixels, fillVal, (width * height * bpp));

        usedPixels = 0;
        initialized = true;

        // create texture with linear filter sampling, no mipmapping
        lod_t lod;
        lod.calculation = LOD_USE_K;
        lod.max_level = 0;
        lod.mag_filter = LOD_MAG_LINEAR;
        lod.min_filter = LOD_MIN_LINEAR;
        lod.l = 0;
        lod.k = 0.f;

        return texture.InitFromMemory(pixels, 
                                      TEXTURE_COMPONENTS_RGBA, 
                                      width, height, 
                                      GS_PSM_32, 
                                      TEXTURE_FUNCTION_MODULATE, 
                                      &lod, 
                                      NULL);
    }

    void ReserveMemForRegions(const u32 numRegions)
    {
        nodes.reserve(nodes.size() + numRegions);
    }

    void Clear(const u8 fillVal)
    {
        nodes.clear();
        if (pixels != NULL)
        {
            memset(pixels, fillVal, width * height * bpp);
        }

        usedPixels = 0;
    }


    Rect4i AllocRegion(const int w, const int h)
    {
        int y;
        int bestWidth;
        int bestHeight;
        int bestIndex;
        Vec3i* node;
        Vec3i* prev;
        Rect4i region = { 0, 0, w, h };
        unsigned int i;

        bestWidth = INT_MAX;
        bestHeight = INT_MAX;
        bestIndex = -1;

        for (i = 0; i < nodes.size(); i++)
        {
            y = fit(i, w, h);
            if (y >= 0)
            {
                node = &nodes[i];
                if (((y + h) < bestHeight) ||
                    ((y + h) == bestHeight && node->z < bestWidth))
                {
                    bestHeight = y + h;
                    bestIndex = i;
                    bestWidth = node->z;
                    region.x = node->x;
                    region.y = y;
                }
            }
        }

        // out of room for texture
        if (bestIndex == -1)
        {
            region.x = -1;
            region.y = -1;
            region.width = 0;
            region.height = 0;
            
            return region;
        }

        Vec3i tmpNode;
        node = &tmpNode;
        node->x = region.x;
        node->y = region.y + h;
        node->z = w;
        nodes.insert(bestIndex, *node);

        for (i = bestIndex + 1; i < nodes.size(); i++)
        {
            node = &nodes[i];
            prev = &nodes[i-1];

            if (node->x < (prev->x + prev->z))
            {
                int shrink = (prev->x + prev->z - node->x);
                node->x += shrink;
                node->z -= shrink;

                if (node->z <= 0)
                {
                    nodes.erase(i);
                    i--;
                }
                else
                {
                    break;
                }
            }
            else
            {
                break;
            }
        }

        merge();
        usedPixels += (w * h);
        return region;
    }

    void SetRegion(const int x, const int y, const int w, const int h, const u8* newData, const int stride)
    {
        //assert(pixels != NULL && "Atlas has no image data assigned to it");

        //assert(x > 0 && y > 0)
        //assert(x < (width - 1))
        //assert((x + w) <= (width - 1))
        //assert(y < (height - 1))
        //assert((y + h) <= (height - 1))
        //assert(newData != NULL)

        // stride is length of a row of pixels, in bytes
        for (int i = 0; i < h; i++)
        {
            memcpy((pixels + ((y + i) * width + x) * bpp),
                   (newData + (i * stride)),
                    w * bpp);
        }
    }

    void SetRegion(const int x, const int y, const int w, const int h, u32 color)
    {
        //assert(pixels != NULL && "Atlas has no image data assigned to it");

        //assert(x > 0 && y > 0)
        //assert(x < (width - 1))
        //assert((x + w) <= (width - 1))
        //assert(y < (height - 1))
        //assert((y + h) <= (height - 1))

        u32* rgbaPixels = (u32*)pixels;

        for (int yi = y; yi < (y + h); yi++)
        {
            for (int xi = x; xi < (x + w); xi++)
            {
                rgbaPixels[xi + yi * width] = color;
            }
        }
    }
    
    bool IsInitialized() const
    {
        return initialized;
    }

    Texture& GetTexture()
    {
        return texture;
    }

private:

    // private copy & assign
    TextureAtlas(const TextureAtlas&);
    TextureAtlas& operator=(const TextureAtlas&);

    Texture texture;

    // nodes of the atlas - each sub region
    Array<Vec3i> nodes;

    // system memory copy of the texture data
    u8* pixels;
    int usedPixels; // allocated surface size in pixels
    int width;
    int height;
    int bpp;    // bytes per pixel (NOT bits!)
    bool initialized;

    int fit(const int index, const int w, const int h)
    {
         //assert(!nodes.isEmpty());  

        const Vec3i* node;
        int x;
        int y;
        int widthLeft;
        int i;

        node = &nodes[index];
        x = node->x;
        y = node->y;
        widthLeft = w;
        i = index;


        if ((x + w) > (width - 1))
        {
            return -1;
        }

        // TODO - necessary? probably not; remove
        y = node->y;

        while (widthLeft > 0)
        {
            node = &nodes[i];

            if (node->y > y)
            {
                y = node->y;
            }

            if ((y+h) > (height - 1))
            {
                return -1;
            }

            widthLeft -= node->z;
            i++;
        }

        return y;
    }

    void merge()
    {
        if (nodes.isEmpty())
        {
            return;
        }

        Vec3i* node;
        Vec3i* next;
        for (u32 i = 0; i < nodes.size() - 1; i++)
        {
            node = &nodes[i];
            next = &nodes[i+1];
            
            if (node->y == next->y)
            {
                node->z  += next->z;
                nodes.erase(i + 1);
                i--;

                if (nodes.isEmpty())
                {
                    break;
                }
            }
        }
    }
};

// STBI image implementation
namespace
{
#define STB_IMAGE_IMPLEMENTATION

// Only need these for now:
#define STBI_ONLY_JPEG
#define STBI_ONLY_PNG
#define STBI_ONLY_TGA

// Don't need HDR nor loading from file.
#define STBI_NO_HDR
#define STBI_NO_LINEAR
#define STBI_NO_STDIO
#define STBI_NO_SIMD

// Use our custom assert:
//#define STBI_ASSERT ps2assert

// Use 128-aligned allocations.
// This will force STBI to use 128bytes aligned allocations for fresh allocs:
//#define STBI_MALLOC(size)          memTagMalloc(MEM_TAG_TEXTURE, (size), 128)
#define STBI_MALLOC(size)          memalign(128, (size))
//#define STBI_REALLOC(ptr, newSize) memTagRealloc(MEM_TAG_TEXTURE, (ptr), (newSize))
#define STBI_REALLOC(ptr, newSize) realloc((ptr), (newSize))
//#define STBI_FREE(ptr)             memTagFree(MEM_TAG_TEXTURE, (ptr))
#define STBI_FREE(ptr)             free((ptr))

// STBI header-only library:
#include "../stb_image.h"
}

struct ImageData
{
	u8*    pixels;
	u32    width;
	u32    height;
	u32    comps;
};

// Loads a JPG, TGA or PNG from a memory buffer with the file contents.
// If `forceRgba` is true, output `image.comps` will always be 4.
static inline bool loadImageFromMemory(const u8* data, uint sizeBytes, ImageData& image, bool forceRgba)
{
    memset(&image, 0, sizeof(image));
    
    if (data == NULL || sizeBytes == 0)
    {
        printf("Invalid data buffer\n");
        return false;
    }

    int w, h, c;
    stbi_uc* pixels = stbi_load_from_memory(data, sizeBytes, &w, &h, &c, (forceRgba ? 4 : 0));

    if (pixels == NULL)
    {
        printf("stbi load failed, error: %s\n", stbi_failure_reason());
        return false;
    }

    // check power of 2 dimensions
    if ((w & (w - 1) != 0) || (h & (h - 1) != 0))
    {
        printf("WARNING: image not power of 2 dims");
    }

    image.pixels = pixels;
    image.width = w;
    image.height = h;
    image.comps = (forceRgba ? 4 : c);

    printf("Loaded image from memory: (w,h,components): %d,%d,%d\n", w, h, c);

    return true;
}

// Frees image data. Should be called on every ImageData instance when it gets disposed.
static inline void imageCleanup(ImageData& image)
{
    if (image.pixels != NULL)
    {
        stbi_image_free(image.pixels);
    }

    memset(&image, 0, sizeof(image));
}


#endif // TEXTUREATLAS_H_INCLUDED
